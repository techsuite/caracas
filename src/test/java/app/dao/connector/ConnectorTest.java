package app.dao.connector;

import app.webscraping.pages.GenericPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mockStatic;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConnectorTest {

  @Autowired
  private Connector connector;

  @MockBean
  private GenericPage genericPage;

//  @Before
//  public void setUp() {
//    //    WebDriverManager.chromedriver().setup();
//  }

  @Test
  public void getConnection_whenCalled_shouldExecuteDriverManagerGetConnectionMethod() {
    try (
      MockedStatic<DriverManager> mockedStatic = mockStatic(DriverManager.class)
    ) {
      this.connector.getConnection();

      mockedStatic.verify(() -> DriverManager.getConnection(Connector.DB_URL));
    }
  }

  @Test
  public void getConnection_whenAnExceptionIsThrown_shouldReturnNull()
    throws Exception {
    try (
      MockedStatic<DriverManager> mockedStatic = mockStatic(DriverManager.class)
    ) {
      mockedStatic
        .when(() -> DriverManager.getConnection(anyString()))
        .thenThrow(SQLException.class);

      Connection output = this.connector.getConnection();

      assertThat(output).isNull();
    }
  }
}
