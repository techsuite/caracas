package app.dao;

import app.model.EnfermedadesModel;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EnfermedadesDAOTest extends GenericDaoTest {

    @Autowired
    private EnfermedadesDAO enfermedadesDAO;


    @Test
    public void insertarEnfermedad_whenCalled_shouldExecuteRunnerUpdate()
            throws Exception {

        EnfermedadesModel enf =new EnfermedadesModel("nombre","spanish");
        enf.setDateNextUpdate(new Timestamp(121,4,7,6,7,1,3));

        Timestamp actual = new Timestamp(System.currentTimeMillis());

        enfermedadesDAO.insertarEnfermedad(enf);

        verify(queryRunner)
                .insert(eq(connection),
                        anyString(),
                        any(ScalarHandler.class),
                        anyString(), any(),
                        any(), any(), anyString(),
                        eq(enf.getFrecUpdateHours()),eq(enf.getDateNextUpdate().toInstant().getEpochSecond()),

                        any(),
                        any(), any(),
                        eq(enf.getFrecUpdateHours()),eq(enf.getFrecUpdateHours()*3600*24+actual.toInstant().getEpochSecond())
                );
    }

    @Test
    public void enfermedadesQueActualizarSp_whenCalled_shouldExecute()
            throws Exception {
        enfermedadesDAO.enfermedadesQueActualizarSp();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void enfermedadesQueActualizarEn_whenCalled_shouldExecute()
            throws Exception {
        enfermedadesDAO.enfermedadesQueActualizarEn();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void getEnfermedadesFromDBSp_whenCalled_shouldExecute()
            throws Exception {
        enfermedadesDAO.getEnfermedadesFromDBSp();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void getEnfermedadesFromDBEn_whenCalled_shouldExecute()
            throws Exception {
        enfermedadesDAO.getEnfermedadesFromDBEn();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void updateFrecUpdate_whenCalled_shouldExecute()
            throws Exception {
        enfermedadesDAO.updateFrecUpdate("nombre", "idioma", 0);

        verify(queryRunner)
                .update(eq(connection), anyString(),eq(0), eq("nombre"), eq("idioma"));
    }

}
