package app.dao;

import app.dao.connector.Connector;
import app.model.MedicamentosModel;
import app.model.response.Metadata;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class  GenericDaoTest {

  @MockBean
  protected Connector connector;

  @MockBean
  protected QueryRunner queryRunner;

  @Mock
  protected Connection connection;

  @Mock
  protected Statement statement;

  @Mock
  protected PreparedStatement preparedStatement;

  @Mock
  protected ResultSet resultSet;

  @Before
  public void setup() throws Exception {
    when(connector.getConnection()).thenReturn(connection);
    when(connection.createStatement()).thenReturn(statement);
    when(connection.prepareStatement(anyString()))
      .thenReturn(preparedStatement);
    when(preparedStatement.executeQuery()).thenReturn(resultSet);
    when(statement.executeQuery(anyString())).thenReturn(resultSet);
  }

  @Mock
  private Metadata metadata;


  @Autowired
  private GenericDAO genericDAO;


  @Test
  public void paginationQuery_go_Correctly(){
    when(metadata.getPageSize()).thenReturn(1);
    when(metadata.getPageNumber()).thenReturn(1);

    assertThat(genericDAO.paginationQuery(metadata)).isEqualTo("LIMIT 1 OFFSET 0 ");

  }
  @Test
  public void idiomaQuery_go_Correctly(){
    String idioma = "dummy";
    assertThat(genericDAO.idiomaQuery(idioma)).isEqualTo("WHERE idioma = 'dummy' ");

  }

  @Test
  public void nombreQuery_go_Correctly(){
    String nombre = "dummy";
    assertThat(genericDAO.nombreQuery(nombre)).isEqualTo("and nombre = 'dummy' ");

  }


  @Test
  public <T> void getPagesFromQuery_go_Correctly() throws Exception{

      genericDAO.getPagesFromQuery(metadata, "dummy_query",MedicamentosModel.class);
      verify(queryRunner).query(eq(connection),anyString(),any(BeanListHandler.class));

  }

  @Test
  public <T> void getPagesWithIdioma_go_Correctly() throws Exception{
    genericDAO.getPagesWithIdioma(metadata,"","",MedicamentosModel.class);
    verify(queryRunner).query(eq(connection),anyString(),any(BeanListHandler.class));

  }

  @Test
  public void numOfRows_go_Correctly() throws Exception{

    when(queryRunner.query(any(Connection.class),anyString(),any())).thenReturn((long) 0 );

    genericDAO.numOfRows("dummy_table", "dummy_idioma");

    verify(queryRunner).query(eq(connection),anyString(),any(ScalarHandler.class));
  }

  @Test
  public void deleteFromQuery_go_Correctly() throws Exception{
    genericDAO.deleteFromQuery("dummyTabla","dummyNombre","dummyIdioma");
    verify(queryRunner).update(eq(connection),anyString());
  }


}
