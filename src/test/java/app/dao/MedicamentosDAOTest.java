
package app.dao;


import app.model.MedicamentosModel;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

import static org.mockito.Mockito.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class MedicamentosDAOTest extends GenericDaoTest {

    @Autowired
    private MedicamentosDAO medicamentosDAO;

    //MIRAR SI NUMERO anyString SON CORRECTOS
    @Test
    public void insertarMedicamento_whenCalled_shouldExecuteRunnerUpdate()
            throws Exception {

        MedicamentosModel med =new MedicamentosModel("nombre","spanish");
        med.setDateNextUpdate(new Timestamp(121,4,7,6,7,1,3));

        Timestamp actual = new Timestamp(System.currentTimeMillis());

        medicamentosDAO.insertarMedicamento(med);

        verify(queryRunner)
                .insert(eq(connection),
                        anyString(),
                        any(ScalarHandler.class),
                        anyString(), any(), any(), any(),
                        any(), any(), anyString(),
                        eq(med.getFrecUpdateHours()),eq(med.getDateNextUpdate().toInstant().getEpochSecond()),

                        any(),any(), any(),
                        any(),any(),
                        eq(med.getFrecUpdateHours()),eq(med.getFrecUpdateHours()*3600*24+actual.toInstant().getEpochSecond())
                );


    }

    @Test
    public void medQueActualizarSp_whenCalled_shouldExecute()
            throws Exception {
        medicamentosDAO.medQueActualizarSp();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void medQueActualizarEn_whenCalled_shouldExecute()
            throws Exception {
        medicamentosDAO.medQueActualizarEn();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void getMedicamentosFromDBSp_whenCalled_shouldExecute()
            throws Exception {
        medicamentosDAO.getMedicamentosFromDBSp();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void getMedicamentosFromDBEn_whenCalled_shouldExecute()
            throws Exception {
        medicamentosDAO.getMedicamentosFromDBEn();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void updateFrecUpdate_whenCalled_shouldExecute()
            throws Exception {
        medicamentosDAO.updateFrecUpdate("nombre", "idioma", 0);

        verify(queryRunner)
                .update(eq(connection), anyString(),eq(0), eq("nombre"), eq("idioma"));
    }

}
