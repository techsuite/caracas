package app.dao;

import app.model.ProdNatModel;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProdNatDAOTest extends GenericDaoTest {

    @Autowired
    private ProdNatDAO prodNatDAO;

    //MIRAR SI NUMERO anyString SON CORRECTOS
    @Test
    public void insertarProdNat_whenCalled_shouldExecuteRunnerUpdate()
            throws Exception {

        ProdNatModel prod =new ProdNatModel("nombre","spanish");
        prod.setDateNextUpdate(new Timestamp(121,4,7,6,7,1,3));

        Timestamp actual = new Timestamp(System.currentTimeMillis());

        prodNatDAO.insertarProdNat(prod);

        verify(queryRunner)
                .insert(eq(connection),
                        anyString(),
                        any(ScalarHandler.class),
                        anyString(), any(), any(), any(),
                        any(), any(), anyString(),
                        eq(prod.getFrecUpdateHours()),eq(prod.getDateNextUpdate().toInstant().getEpochSecond()),

                        any(),any(), any(),
                        any(), any(),
                        eq(prod.getFrecUpdateHours()),eq(prod.getFrecUpdateHours()*3600*24+actual.toInstant().getEpochSecond())
                );


    }

    @Test
    public void prodNatQueActualizarSp_whenCalled_shouldExecute()
            throws Exception {
        prodNatDAO.prodNatQueActualizarSp();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void prodNatQueActualizarEn_whenCalled_shouldExecute()
            throws Exception {
        prodNatDAO.prodNatQueActualizarEn();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void getProdNatFromDBSp_whenCalled_shouldExecute()
            throws Exception {
        prodNatDAO.getProdNatFromDBSp();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void getProdNatFromDBEn_whenCalled_shouldExecute()
            throws Exception {
        prodNatDAO.getProdNatFromDBEn();

        verify(queryRunner)
                .query(eq(connection), anyString(), any(BeanListHandler.class));
    }

    @Test
    public void updateFrecUpdate_whenCalled_shouldExecute()
            throws Exception {
        prodNatDAO.updateFrecUpdate("nombre", "idioma", 0);

        verify(queryRunner)
                .update(eq(connection), anyString(),eq(0), eq("nombre"), eq("idioma"));
    }

}
