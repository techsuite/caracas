package app.controller;

import app.dao.MedicamentosDAO;
import app.factory.MedicamentosResponseFactory;
import app.model.response.ListMedicamentosResponse;
import app.model.response.MedicamentoResponse;
import app.model.response.Metadata;
import com.google.gson.Gson;
import one.util.streamex.EntryStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MedicamentosControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicamentosDAO medicamentosDAO;

    @MockBean
    private MedicamentosResponseFactory medicamentosResponseFactory;


    @Test
    public void medicamentos_when_called_Correctly_OneElement() throws Exception {


        List<MedicamentoResponse> dummyMedicamentos = List.of(new MedicamentoResponse("nombre1",
                "contraind1", "pos1", "prospecto1", "composicion1", "url",
                 "spanish"));
        Metadata metadata = new Metadata(1, 1, 1);
        ListMedicamentosResponse medRes = new ListMedicamentosResponse(dummyMedicamentos, metadata);

        when(medicamentosDAO.getPagesWithIdioma(metadata, "spanish", "medicamentos", MedicamentoResponse.class))
                .thenReturn(dummyMedicamentos);
        when(medicamentosDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(medicamentosResponseFactory.newMedicamentosResponse(any(), any())).thenReturn(medRes);
        mockMvc
                .perform(get(String.format("/medicamentos?PageNumber=%d&PageSize=%d&idioma=spanish", metadata.getPageNumber(), metadata.getPageSize())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.medicamentos", hasSize(1)))
                .andExpect(jsonPath("$.metadata.pageNumber", is(1)));
    }

    @Test
    public void medicamentos_when_called_firstIF_Correctly_ingles() throws Exception {

        List<MedicamentoResponse> dummyMedicamentos = List.of(new MedicamentoResponse("nombre1",
                "contraind1", "pos1", "prospecto1", "composicion1", "url", "english"));
        Metadata metadata = new Metadata(1, 1, 1);
        ListMedicamentosResponse medRes = new ListMedicamentosResponse(dummyMedicamentos, metadata);

        when(medicamentosDAO.getPagesWithIdioma(metadata, "english", "medicamentos", MedicamentoResponse.class)).thenReturn(dummyMedicamentos);
        when(medicamentosDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(medicamentosResponseFactory.newMedicamentosResponse(any(), any())).thenReturn(medRes);
        mockMvc
                .perform(get(String.format("/medicamentos?PageNumber=%d&PageSize=%d&idioma=english", metadata.getPageNumber(), metadata.getPageSize())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.medicamentos", hasSize(1)))
                .andExpect(jsonPath("$.metadata.pageNumber", is(1)));

    }

    @Test
    public void medicamentos_when_called_firstIF_WrongFFF() throws Exception {
        mockMvc
                .perform(get("/medicamentos?PageNumber=0&PageSize=0&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void medicamentos_when_called_firstIF_WrongTFF() throws Exception {
        mockMvc
                .perform(get("/medicamentos?PageNumber=1&PageSize=0&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void medicamentos_when_called_firstIF_WrongTTF() throws Exception {
        mockMvc
                .perform(get("/medicamentos?PageNumber=1&PageSize=1&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void medicamentos_when_called_secondIF_WrongTF() throws Exception{
        List<MedicamentoResponse> dummyMedicamentos = List.of(new MedicamentoResponse("nombre1",
                "contraind1","pos1","prospecto1","composicion1","url",
               "spanish"));
        Metadata metadata = new Metadata(2,1,1);
        ListMedicamentosResponse medRes = new ListMedicamentosResponse(dummyMedicamentos,metadata);
        when(medicamentosDAO.getPagesWithIdioma(metadata,"español","medicamentos",MedicamentoResponse.class)).thenReturn(dummyMedicamentos);
        when(medicamentosDAO.numOfRows(anyString(),anyString())).thenReturn(1);
        when(medicamentosResponseFactory.newMedicamentosResponse(any(),any())).thenReturn(medRes);
        mockMvc
                .perform(get(String.format("/medicamentos?PageNumber=%d&PageSize=%d&idioma=spanish",metadata.getPageNumber(),metadata.getPageSize())))
                .andExpect(status().isBadRequest());


    }
    @Test
    public void medicamentos_when_called_Correctly_TwoElements()
            throws Exception {

        Metadata metadata = new Metadata(1, 2, 1);
        List<MedicamentoResponse> dummyMedicamentos = List.of(
                new MedicamentoResponse("nombre1", "contraindicaciones1", "posologia1",
                        "prospecto1", "composicion1", "url1","spanish"),
                new MedicamentoResponse("nombre2", "contraindicaciones2", "posologia2",
                        "prospecto2", "composicion2", "url2", "spanish")
        );
        ListMedicamentosResponse medRes = new ListMedicamentosResponse(dummyMedicamentos, metadata);

        when(medicamentosDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(medicamentosResponseFactory.newMedicamentosResponse(any(), any())).thenReturn(medRes);

        when(medicamentosDAO.getPagesWithIdioma(metadata, "spanish", "medicamentos", MedicamentoResponse.class))
                .thenReturn(dummyMedicamentos);

        String outputBody = mockMvc
                .perform(get("/medicamentos?PageNumber=1&PageSize=2&idioma=spanish"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.medicamentos", hasSize(dummyMedicamentos.size())))
                .andReturn()
                .getResponse()
                .getContentAsString();

        EntryStream
                .of(new Gson().fromJson(outputBody,
                        ListMedicamentosResponse.class).getMedicamentos())
                .forKeyValue((index, output) ->
                        assertThat(output).isEqualTo(dummyMedicamentos.get(index)));

    }
    @Test
    public void medicamentos_when_called_with_PageSize_Null() throws Exception {
        mockMvc
                .perform(get("/medicamentos?PageSize=1&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void medicamentos_when_called_firstIF_PageNumber_Null() throws Exception {
        mockMvc
                .perform(get("/medicamentos?PageNumber=1&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void medicamentos_when_called_firstIF_PageSize_Null_and_PageNumber_Null() throws Exception {
        mockMvc
                .perform(get("/medicamentos?idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void medicamentos_when_called_firstIF_Idioma_Null() throws Exception {
        mockMvc
                .perform(get("/medicamentos?PageNumber=1&PageSize=1"))
                .andExpect(status().isBadRequest());

    }

}

