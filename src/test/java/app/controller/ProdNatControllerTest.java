package app.controller;

import app.dao.ProdNatDAO;
import app.factory.ProdNatResponseFactory;
import app.model.response.ListProdNatsResponse;
import app.model.response.Metadata;
import app.model.response.ProdNatResponse;
import com.google.gson.Gson;
import one.util.streamex.EntryStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProdNatControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProdNatDAO prodNatDAO;

    @MockBean
    private ProdNatResponseFactory prodNatResponseFactory;


    @Test
    public void prodNat_when_called_Correctly_OneElement() throws Exception {

        List<ProdNatResponse> dummyProdNat = List.of(new ProdNatResponse("nombre1",
                "contraind1","pos1","prospecto1","composicion1","url",
                "spanish"));
        Metadata metadata = new Metadata(1,1,1);
        ListProdNatsResponse prodNatRes = new ListProdNatsResponse(dummyProdNat,metadata);

        when(prodNatDAO.getPagesWithIdioma(metadata,"spanish","productos_naturales",ProdNatResponse.class)).thenReturn(dummyProdNat);
        when(prodNatDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(prodNatResponseFactory.newProdNatResponse(any(),any())).thenReturn(prodNatRes);
        mockMvc
                .perform(get(String.format("/prodNat?PageNumber=%d&PageSize=%d&idioma=spanish",metadata.getPageNumber(),metadata.getPageSize())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productos_naturales", hasSize(1)))
                .andExpect(jsonPath("$.metadata.pageNumber", is(1)));
    }
    @Test
    public void prodNat_when_called_firstIF_Correctly_ingles() throws Exception{

        List<ProdNatResponse> dummyProdNat = List.of(new ProdNatResponse("nombre1",
                "contraind1","pos1","prospecto1","composicion1","url",
                "english"));
        Metadata metadata = new Metadata(1,1,1);
        ListProdNatsResponse prodNatRes = new ListProdNatsResponse(dummyProdNat,metadata);

        when(prodNatDAO.getPagesWithIdioma(metadata,"english","productos_naturales",ProdNatResponse.class)).thenReturn(dummyProdNat);
        when(prodNatDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(prodNatResponseFactory.newProdNatResponse(any(),any())).thenReturn(prodNatRes);
        mockMvc
                .perform(get(String.format("/prodNat?PageNumber=%d&PageSize=%d&idioma=english",metadata.getPageNumber(),metadata.getPageSize())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productos_naturales", hasSize(1)))
                .andExpect(jsonPath("$.metadata.pageNumber", is(1)));

    }

    @Test
    public void prodNat_when_called_firstIF_WrongFFF() throws Exception{
        mockMvc
                .perform(get("/prodNat?PageNumber=0&PageSize=0&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void prodNat_when_called_firstIF_WrongTFF() throws Exception{
        mockMvc
                .perform(get("/prodNat?PageNumber=1&PageSize=0&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void prodNat_when_called_firstIF_WrongTTF() throws Exception{
        mockMvc
                .perform(get("/prodNat?PageNumber=1&PageSize=1&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void prodNat_when_called_secondIF_WrongTF() throws Exception{
        List<ProdNatResponse> dummyProdNat = List.of(new ProdNatResponse("nombre1",
                "contraind1","pos1","prospecto1","composicion1","url",
                "español"));
        Metadata metadata = new Metadata(2,1,1);
        ListProdNatsResponse prodNatRes = new ListProdNatsResponse(dummyProdNat,metadata);

        when(prodNatDAO.getPagesWithIdioma(metadata,"spanish","productos_naturales",ProdNatResponse.class)).thenReturn(dummyProdNat);
        when(prodNatDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(prodNatResponseFactory.newProdNatResponse(any(),any())).thenReturn(prodNatRes);
        mockMvc
                .perform(get(String.format("/prodNat?PageNumber=%d&PageSize=%d&idioma=spanish",metadata.getPageNumber(),metadata.getPageSize())))
                .andExpect(status().isBadRequest());


    }
    @Test
    public void prodNat_when_called_secondIF_FF() throws Exception{
        List<ProdNatResponse> dummyProdNat = List.of(new ProdNatResponse("nombre1",
                "contraind1","pos1","prospecto1","composicion1","url",
                "español"));
        Metadata metadata = new Metadata(2,1,1);
        ListProdNatsResponse prodNatRes = new ListProdNatsResponse(dummyProdNat,metadata);

        when(prodNatDAO.getPagesWithIdioma(metadata,"spanish","productos_naturales",ProdNatResponse.class)).thenReturn(dummyProdNat);
        when(prodNatDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(prodNatResponseFactory.newProdNatResponse(any(),any())).thenReturn(prodNatRes);
        mockMvc
                .perform(get(String.format("/prodNat?PageNumber=%d&PageSize=%d&idioma=spanish",metadata.getPageNumber(),metadata.getPageSize())))
                .andExpect(status().isBadRequest());


    }
    @Test
    public void ProdNat_when_called_Correctly_TwoElements()
            throws Exception {

        Metadata metadata = new Metadata(1,2,1);
        List<ProdNatResponse> dummyProdNat= List.of(
                new ProdNatResponse("nombre1","contraindicaciones1","posologia1",
                        "prospecto1","composicion1","url1","spanish"),
                new ProdNatResponse("nombre2","contraindicaciones2","posologia2",
                        "prospecto2","composicion2","url2","spanish")
        );
        ListProdNatsResponse prodNatRes = new ListProdNatsResponse(dummyProdNat,metadata);

        when(prodNatDAO.getPagesWithIdioma(metadata,"spanish","productos_naturales",ProdNatResponse.class))
                .thenReturn(dummyProdNat);
        when(prodNatDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(prodNatResponseFactory.newProdNatResponse(any(),any())).thenReturn(prodNatRes);

        String outputBody = mockMvc
                .perform(get("/prodNat?PageNumber=1&PageSize=2&idioma=spanish"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productos_naturales", hasSize(dummyProdNat.size())))
                .andReturn()
                .getResponse()
                .getContentAsString();

        EntryStream
                .of(new Gson().fromJson(outputBody,
                        ListProdNatsResponse.class).getProductos_naturales())
                .forKeyValue((index, output) ->
                        assertThat(output).isEqualTo(dummyProdNat.get(index)));

    }
    @Test
    public void prodNat_when_called_with_PageSize_Null() throws Exception {
        mockMvc
                .perform(get("/prodNat?PageSize=1&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void prodNat_when_called_firstIF_PageNumber_Null() throws Exception {
        mockMvc
                .perform(get("/prodNat?PageNumber=1&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void prodNat_when_called_firstIF_PageSize_Null_and_PageNumber_Null() throws Exception {
        mockMvc
                .perform(get("/prodNat?idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void prodNat_when_called_firstIF_Idioma_Null() throws Exception {
        mockMvc
                .perform(get("/prodNat?PageNumber=1&PageSize=1"))
                .andExpect(status().isBadRequest());

    }

}

