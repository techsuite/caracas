
package app.controller;

import app.dao.EnfermedadesDAO;
import app.factory.EnfermedadesResponseFactory;
import app.model.response.EnfermedadResponse;
import app.model.response.ListEnfermedadesResponse;
import app.model.response.Metadata;
import com.google.gson.Gson;
import one.util.streamex.EntryStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EnfermedadesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EnfermedadesDAO enfermedadesDAO;

    @MockBean
    private EnfermedadesResponseFactory enfermedadesResponseFactory;


    @Test
    public void enfermedades_when_called_Correctly_OneElement() throws Exception {


        List<EnfermedadResponse> dummyEnfermedades = List.of(new EnfermedadResponse("nombre1",
                "sintomas1","recom1","url","spanish"));
        Metadata metadata = new Metadata(1,1,1);
        ListEnfermedadesResponse enfermedadesResponse = new ListEnfermedadesResponse(dummyEnfermedades,metadata);

        when(enfermedadesDAO.getPagesWithIdioma(metadata,"spanish","enfermedades",EnfermedadResponse.class)).thenReturn(dummyEnfermedades);
        when(enfermedadesDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(enfermedadesResponseFactory.newEnfermedadesResponse(any(),any())).thenReturn(enfermedadesResponse);
        mockMvc
                .perform(get(String.format("/enfermedades?PageNumber=%d&PageSize=%d&idioma=spanish",metadata.getPageNumber(),metadata.getPageSize())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.enfermedades", hasSize(1)))
                .andExpect(jsonPath("$.metadata.pageNumber", is(1)));
    }
    @Test
    public void enfermedades_when_called_Correctly_OneElement_ingles() throws Exception {

        List<EnfermedadResponse> dummyEnfermedades = List.of(new EnfermedadResponse("nombre1",
                "sintomas1","recom1","url","english"));
        Metadata metadata = new Metadata(1,1,1);
        ListEnfermedadesResponse enfermedadesResponse = new ListEnfermedadesResponse(dummyEnfermedades,metadata);

        when(enfermedadesDAO.getPagesWithIdioma(metadata,"inglés","enfermedades",EnfermedadResponse.class)).thenReturn(dummyEnfermedades);
        when(enfermedadesDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(enfermedadesResponseFactory.newEnfermedadesResponse(any(),any())).thenReturn(enfermedadesResponse);
        mockMvc
                .perform(get(String.format("/enfermedades?PageNumber=%d&PageSize=%d&idioma=english",metadata.getPageNumber(),metadata.getPageSize())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.enfermedades", hasSize(1)))
                .andExpect(jsonPath("$.metadata.pageNumber", is(1)));
    }

    @Test
    public void enfermedades_when_called_firstIF_WrongFFF() throws Exception{
        mockMvc
                .perform(get("/enfermedades?PageNumber=0&PageSize=0&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void enfermedades_when_called_firstIF_WrongTFF() throws Exception{
        mockMvc
                .perform(get("/enfermedades?PageNumber=1&PageSize=0&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void enfermedades_when_called_firstIF_WrongTTF() throws Exception{
        mockMvc
                .perform(get("/enfermedades?PageNumber=1&PageSize=1&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void enfermedades_when_called_secondIF_WrongTF() throws Exception{
        List<EnfermedadResponse> dummyEnfermedades = List.of(new EnfermedadResponse("nombre1",
                "sintomas1","recom1","url","spanish"));
        Metadata metadata = new Metadata(2,1,1);
        ListEnfermedadesResponse enfermedadesResponse = new ListEnfermedadesResponse(dummyEnfermedades,metadata);

        when(enfermedadesDAO.getPagesWithIdioma(metadata,"español","enfermedades",EnfermedadResponse.class)).thenReturn(dummyEnfermedades);
        when(enfermedadesDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(enfermedadesResponseFactory.newEnfermedadesResponse(any(),any())).thenReturn(enfermedadesResponse);
        mockMvc
                .perform(get(String.format("/enfermedades?PageNumber=%d&PageSize=%d&idioma=spanish",metadata.getPageNumber(),metadata.getPageSize())))
                .andExpect(status().isBadRequest());


    }

    @Test
    public void enfermedades_when_called_Correctly_TwoElements()
            throws Exception {

        Metadata metadata = new Metadata(1,2,1);
        List<EnfermedadResponse> dummyEnfermedades = List.of(
                new EnfermedadResponse("nombre1","sintomas1","recomendaciones1","url1",
                        "spanish"),
                new EnfermedadResponse("nombre2","sintomas2","recomendaciones2","url2",
                        "spanish"));
        ListEnfermedadesResponse enfRes = new ListEnfermedadesResponse(dummyEnfermedades,metadata);

        when(enfermedadesDAO.numOfRows(anyString(), anyString())).thenReturn(1);
        when(enfermedadesResponseFactory.newEnfermedadesResponse(any(),any())).thenReturn(enfRes);

        when(enfermedadesDAO.getPagesWithIdioma(metadata,"spanish", "enfermedades", EnfermedadResponse.class))
                .thenReturn(dummyEnfermedades);

        String outputBody = mockMvc
                .perform(get("/enfermedades?PageNumber=1&PageSize=2&idioma=spanish"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.enfermedades", hasSize(dummyEnfermedades.size())))
                .andReturn()
                .getResponse()
                .getContentAsString();

        EntryStream
                .of(new Gson().fromJson(outputBody,
                        ListEnfermedadesResponse.class).getEnfermedades())
                .forKeyValue((index, output) ->
                        assertThat(output).isEqualTo(dummyEnfermedades.get(index)));

    }
    @Test
    public void enfermedades_when_called_with_PageSize_Null() throws Exception {
        mockMvc
                .perform(get("/enfermedades?PageSize=1&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void enfermedades_when_called_firstIF_PageNumber_Null() throws Exception {
        mockMvc
                .perform(get("/enfermedades?PageNumber=1&idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void enfermedades_when_called_firstIF_PageSize_Null_and_PageNumber_Null() throws Exception {
        mockMvc
                .perform(get("/enfermedades?idioma=dummy"))
                .andExpect(status().isBadRequest());

    }
    @Test
    public void enfermedades_when_called_firstIF_Idioma_Null() throws Exception {
        mockMvc
                .perform(get("/enfermedades?PageNumber=1&PageSize=1"))
                .andExpect(status().isBadRequest());

    }
}