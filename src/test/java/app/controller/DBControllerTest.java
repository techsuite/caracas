package app.controller;

import app.dao.EnfermedadesDAO;
import app.dao.MedicamentosDAO;
import app.dao.ProdNatDAO;
import app.service.ChronoService;
import app.service.EnfermedadesService;
import app.service.MedicamentosService;
import app.service.ProdNatService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DBControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EnfermedadesDAO enfermedadesDAO;

    @MockBean
    private MedicamentosDAO medicamentosDAO;

    @MockBean
    private ProdNatDAO prodNatDAO;

    @MockBean
    private ProdNatService prodNatService;

    @MockBean
    private MedicamentosService medicamentosService;

    @MockBean
    private EnfermedadesService enfermedadesService;
    
    @MockBean
    private ChronoService chronoService;

    @Test
    public void borrarMedicamento_when_called_should_go_correctly() throws Exception {
        mockMvc
                .perform(delete(String.format("/borrarMedicamento?nombre=dummynombre&idioma=dummyidioma")))
                .andExpect(status().isOk());

    }
    @Test
    public void borrarEnferemedad_when_called_should_go_correctly() throws Exception {
        mockMvc
                .perform(delete(String.format("/borrarEnfermedad?nombre=dummynombre&idioma=dummyidioma")))
                .andExpect(status().isOk());

    }
    @Test
    public void borrarprodNat_when_called_should_go_correctly() throws Exception {
        mockMvc
                .perform(delete(String.format("/borrarProdNat?nombre=dummynombre&idioma=dummyidioma")))
                .andExpect(status().isOk());

    }

    @Test
    public void editarMedicamento__when_called_should_go_correctly() throws Exception {
        mockMvc
                .perform(post(String.format("/editarMedicamento?nombre=dummynombre&idioma=dummyidioma&horas=24")))
                .andExpect(status().isOk());

    }

    @Test
    public void editarEnfermedad__when_called_should_go_correctly() throws Exception {
        mockMvc
                .perform(post(String.format("/editarEnfermedad?nombre=dummynombre&idioma=dummyidioma&horas=24")))
                .andExpect(status().isOk());

    }

    @Test
    public void editarProdNat__when_called_should_go_correctly() throws Exception {
        mockMvc
                .perform(post(String.format("/editarProdNat?nombre=dummynombre&idioma=dummyidioma&horas=24")))
                .andExpect(status().isOk());

    }

    @Test
    public void actualizarProdNat__when_called_should_go_correctly() throws Exception {
        mockMvc
                .perform(post(String.format("/actualizarProdNat?nombre=dummynombre&idioma=dummyidioma&horas=24&fecha=2021-04-30 21:47:47.0")))
                .andExpect(status().isOk());

    }

    @Test
    public void actualizarEnfermedad__when_called_should_go_correctly() throws Exception {
        mockMvc
                .perform(post(String.format("/actualizarEnfermedad?nombre=dummynombre&idioma=dummyidioma&horas=24&fecha=2021-04-30 21:47:47.0")))
                .andExpect(status().isOk());

    }

    @Test
    public void actualizarMedicamento__when_called_should_go_correctly() throws Exception {
        mockMvc
                .perform(post(String.format("/actualizarMedicamento?nombre=dummynombre&idioma=dummyidioma&horas=24&fecha=2021-04-30 21:47:47.0")))
                .andExpect(status().isOk());

    }

    @Test
    public void actualizartodo__when_called_should_go_correctly() throws Exception {
        mockMvc
                .perform(post(String.format("/actualizarTodo")))
                .andExpect(status().isOk());

    }


}



