
package app.webscraping;

import app.factory.WebDriverFactory;
import org.junit.Before;
import org.mockito.Mock;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class GenericWebScrapingPageTest {

  @MockBean
  protected WebDriverFactory webDriverFactory;

  @Mock
  protected WebDriver webDriver;

  @Mock
  protected WebElement webElement;

  @Mock
  protected WebDriver.TargetLocator targetLocator;

  @Mock
  private WebDriverWait webDriverWait;

  @Before
  public void setup() {

    Set<String> windows = new HashSet<>();
    windows.add("window1");
    windows.add("window2");

    when(webDriverFactory.newChromeDriver()).thenReturn(webDriver);

    when(webDriver.findElement(any())).thenReturn(webElement);
    when(webDriver.switchTo()).thenReturn(targetLocator);
    when(webDriver.getWindowHandles()).thenReturn(windows);
    when(webDriverFactory.newWebDriverWait(any(), any())).thenReturn(webDriverWait);
    when(webDriverWait.until(any())).thenReturn(webElement);
    
//    when(webElement.getText()).thenReturn("adverse effects");
  }
}