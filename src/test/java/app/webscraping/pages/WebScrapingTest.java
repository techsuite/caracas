package app.webscraping.pages;

import app.factory.WebDriverFactory;
import app.model.EnfermedadesModel;
import app.model.MedicamentosModel;
import app.model.ProdNatModel;
import app.service.IOService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileNotFoundException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WebScrapingTest
  extends app.webscraping.GenericWebScrapingPageTest {

  @Autowired
  private WebScraping webScraper;


  @MockBean
  private IOService ioService;

  @Mock
  private WebDriverFactory factory;



  @Test
  public void test_getDrugs_spanish() throws FileNotFoundException {

    MedicamentosModel med1 = new MedicamentosModel("med1","spanish");
    med1.setComposicion("texto");
    med1.setContraind("texto");
    med1.setPosologia("texto");
    med1.setUrl("url");
    med1.setProspecto("url");

    List<MedicamentosModel> meds = List.of(med1);

   when(ioService.getNames(anyString()))
      .thenReturn(List.of("med1"));
    when(webScraper.getText(eq(webDriver),anyString()))
      .thenReturn("texto");
    when(webDriver.getCurrentUrl())
      .thenReturn("url");
    
      

    List<MedicamentosModel> output = webScraper.getDrugs(webDriver,"spanish", meds);
    assertThat(output.get(0)).isEqualTo(med1);
  }

  
  @Test
  public void test_getDrugs_english() throws FileNotFoundException {

    MedicamentosModel med1 = new MedicamentosModel("med1","english");
    med1.setComposicion("med1 composition:");
    med1.setContraind("texto");
    med1.setPosologia("texto");
    med1.setUrl("url");
    med1.setProspecto("url");

    List<MedicamentosModel> meds = List.of(med1);

   when(ioService.getNames(anyString()))
      .thenReturn(List.of("med1"));
    when(webScraper.getText(eq(webDriver),anyString()))
      .thenReturn("texto");
    when(webDriver.getCurrentUrl())
      .thenReturn("url");
    
      

    List<MedicamentosModel> output = webScraper.getDrugs(webDriver,"english", meds);
    assertThat(output.get(0)).isEqualTo(med1);
  }

  /*public void test_getDrugs_fail() throws FileNotFoundException {

   MedicamentosModel med1 = new MedicamentosModel("med1","spanish");
   LinkedList<MedicamentosModel> dummyEnfSp = (LinkedList) List.of(med1);
   
   when(dummyEnfSp.addLast(any()))
      .thenThrow(Exception.class );
    
    List<MedicamentosModel> output = webScraper.getDrugs(webDriver,"spanish");
  }*/


  @Test
  public void test_getHerbs() throws FileNotFoundException {

    ProdNatModel herb1 = new ProdNatModel("herb1","spanish");
    herb1.setComposicion("texto");
    herb1.setContraind("texto");
    herb1.setPosologia("texto");
    herb1.setUrl("url");
    herb1.setProspecto("url");

    List<ProdNatModel> prods = List.of(herb1);

   when(ioService.getNames(anyString()))
      .thenReturn(List.of("herb1"));
    when(webScraper.getText(eq(webDriver),anyString()))
      .thenReturn("texto");
    when(webDriver.getCurrentUrl())
      .thenReturn("url");
    
      

    List<ProdNatModel> output = webScraper.getHerbs(webDriver,"spanish", prods);
    assertThat(output.get(0)).isEqualTo(herb1);
  }

  @Test
  public void test_getIllnesses() throws FileNotFoundException {

    EnfermedadesModel illness = new EnfermedadesModel("ill","spanish");
    illness.setSintomas("texto");
    illness.setRecomendaciones("texto");
    illness.setUrl("url");
   
    List<EnfermedadesModel> ills = List.of(illness);
   

   when(ioService.getNames(anyString()))
      .thenReturn(List.of("ill"));
    when(webScraper.getText(eq(webDriver),anyString()))
      .thenReturn("texto");
    when(webDriver.getCurrentUrl())
      .thenReturn("url");
    
      

    List<EnfermedadesModel> output = webScraper.getIllnesses(webDriver,"spanish", ills);
    assertThat(output.get(0)).isEqualTo(illness);
  }

}
