package app.service;


import app.dao.EnfermedadesDAO;
import app.model.EnfermedadesModel;
import app.model.MedicamentosModel;
import app.webscraping.pages.GenericPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EnfermedadesServiceTest {

    @Autowired
    private EnfermedadesService enfermedadesService;

    @MockBean
    private GenericPage page;

    @MockBean
    private EnfermedadesDAO enfermedadesDAO;

    @Test
    public void actualizarEnfermedades_whenCalled_shouldExecute()
            throws Exception {
        EnfermedadesModel enfSp1 = new EnfermedadesModel("1", "spanish");
        EnfermedadesModel enfSp2 = new EnfermedadesModel("2", "spanish");
        EnfermedadesModel enfEn1= new EnfermedadesModel("1", "english");
        EnfermedadesModel enfEn2= new EnfermedadesModel("2", "english");
        List<EnfermedadesModel> dummyEnfSp = List.of(enfSp1,enfSp2);
        List<EnfermedadesModel> dummyEnfEn = List.of(enfEn1,enfEn2);

        when(page.getIllnessesList(eq("spanish"), any())).thenReturn(dummyEnfSp);
        when(page.getIllnessesList(eq("english"), any())).thenReturn(dummyEnfEn);

        enfermedadesService.actualizarEnfermedades();

        verify(enfermedadesDAO).insertarEnfermedad(enfSp1);
        verify(enfermedadesDAO).insertarEnfermedad(enfSp2);
        verify(enfermedadesDAO).insertarEnfermedad(enfEn1);
        verify(enfermedadesDAO).insertarEnfermedad(enfEn2);
    }


    @Test
    public void actualizarTodasEnfermedadesDB_whenCalled_shouldExecute()
            throws Exception {
        EnfermedadesModel enfSp1 = new EnfermedadesModel("1", "spanish");
        EnfermedadesModel enfSp2 = new EnfermedadesModel("2", "spanish");
        EnfermedadesModel enfEn1= new EnfermedadesModel("1", "english");
        EnfermedadesModel enfEn2= new EnfermedadesModel("2", "english");
        List<EnfermedadesModel> dummyEnfSp = List.of(enfSp1,enfSp2);
        List<EnfermedadesModel> dummyEnfEn = List.of(enfEn1,enfEn2);

        when(page.getIllnessesList(eq("spanish"), any())).thenReturn(dummyEnfSp);
        when(page.getIllnessesList(eq("english"), any())).thenReturn(dummyEnfEn);

        enfermedadesService.actualizarTodasEnfermedadesDB();

        verify(enfermedadesDAO).insertarEnfermedad(enfSp1);
        verify(enfermedadesDAO).insertarEnfermedad(enfSp2);
        verify(enfermedadesDAO).insertarEnfermedad(enfEn1);
        verify(enfermedadesDAO).insertarEnfermedad(enfEn2);
    }

    @Test
    public void actualizarElem_whenCalled_shouldExecute()
            throws Exception {
        EnfermedadesModel enfSp1 = new EnfermedadesModel("1", "spanish");
        Timestamp time = new Timestamp(System.currentTimeMillis());
        enfSp1.setFrecUpdateHours(0);
        enfSp1.setDateNextUpdate(time);

        List<EnfermedadesModel> dummyEnfSp = List.of(enfSp1);

        when(page.getIllnessesList(eq("spanish"), any())).thenReturn(dummyEnfSp);

        enfermedadesService.actualizarElem("1","spanish",0, time);

        verify(enfermedadesDAO).insertarEnfermedad(enfSp1);
    }

}
