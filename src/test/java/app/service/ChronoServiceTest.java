package app.service;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChronoServiceTest {

    @Autowired
    private ChronoService chronoService;

    @MockBean
    private MedicamentosService medicamentosService;

    @MockBean
    private EnfermedadesService enfermedadesService;

    @MockBean
    private ProdNatService prodNatService;


    @Test
    public void actualizarDB_whenCalled_shouldExecute() throws Exception {
        chronoService.actualizarDB();

        verify(medicamentosService).actualizarMedicamentos();
        verify(enfermedadesService).actualizarEnfermedades();
        verify(prodNatService).actualizarProdNat();
    }

    @Test
    public void updateAllDB_whenCalled_shouldExecute() throws Exception {
        chronoService.updateAllDB();

        verify(medicamentosService).actualizarTodosMedicamentosDB();
        verify(enfermedadesService).actualizarTodasEnfermedadesDB();
        verify(prodNatService).actualizarTodosProdNatDB();
    }

}
