package app.service;

import app.dao.MedicamentosDAO;
import app.model.EnfermedadesModel;
import app.model.MedicamentosModel;
import app.webscraping.pages.GenericPage;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MedicamentosServiceTest {

  @Autowired
  private MedicamentosService medicamentosService;

  @MockBean
  private GenericPage page;

  @MockBean
  private MedicamentosDAO medicamentosDAO;

  @Test
  public void actualizarMedicamentos_whenCalled_shouldUpdate()
          throws Exception {
    MedicamentosModel medSp1 = new MedicamentosModel("1", "spanish");
    MedicamentosModel medSp2 = new MedicamentosModel("2", "spanish");
    MedicamentosModel medEn1= new MedicamentosModel("1", "english");
    MedicamentosModel medEn2= new MedicamentosModel("2", "english");

    List<MedicamentosModel> dummyMedSp = List.of(medSp1,medSp2);
    List<MedicamentosModel> dummyMedEn = List.of(medEn1,medEn2);

   // List<String> nombreMedEn = getListNames(drugsEn);

    when(page.getDrugsList(eq("spanish"), any())).thenReturn(dummyMedSp);
    when(page.getDrugsList(eq("english"), any())).thenReturn(dummyMedEn);

    medicamentosService.actualizarMedicamentos();

    verify(medicamentosDAO).insertarMedicamento(medSp1);
    verify(medicamentosDAO).insertarMedicamento(medSp2);
    verify(medicamentosDAO).insertarMedicamento(medEn1);
    verify(medicamentosDAO).insertarMedicamento(medEn2);
  }


  @Test
  public void actualizarTodosMedicamentosDB_whenCalled_shouldUpdate()
          throws Exception {
    MedicamentosModel medSp1 = new MedicamentosModel("1", "spanish");
    MedicamentosModel medSp2 = new MedicamentosModel("2", "spanish");
    MedicamentosModel medEn1= new MedicamentosModel("1", "english");
    MedicamentosModel medEn2= new MedicamentosModel("2", "english");

    List<MedicamentosModel> dummyMedSp = List.of(medSp1,medSp2);
    List<MedicamentosModel> dummyMedEn = List.of(medEn1,medEn2);

    when(page.getDrugsList(eq("spanish"), any())).thenReturn(dummyMedSp);
    when(page.getDrugsList(eq("english"), any())).thenReturn(dummyMedEn);

    medicamentosService.actualizarTodosMedicamentosDB();

    verify(medicamentosDAO).insertarMedicamento(medSp1);
    verify(medicamentosDAO).insertarMedicamento(medSp2);
    verify(medicamentosDAO).insertarMedicamento(medEn1);
    verify(medicamentosDAO).insertarMedicamento(medEn2);
  }

  @Test
  public void actualizarElem_whenCalled_shouldExecute()
          throws Exception {
    MedicamentosModel medSp1 = new MedicamentosModel("1", "spanish");
    Timestamp time = new Timestamp(System.currentTimeMillis());
    medSp1.setFrecUpdateHours(0);
    medSp1.setDateNextUpdate(time);

    List<MedicamentosModel> dummyMedSp = List.of(medSp1);

    when(page.getDrugsList(eq("spanish"), any())).thenReturn(dummyMedSp);

    medicamentosService.actualizarElem("1","spanish",0, time);

    verify(medicamentosDAO).insertarMedicamento(medSp1);
  }
}
