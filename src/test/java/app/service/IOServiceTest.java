package app.service;

import app.factory.WebDriverFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IOServiceTest {

    @MockBean
    private WebDriverFactory webDriverFactory;

    @Autowired
    private IOService ioService;

    @MockBean
    private Scanner scanner;

    @Mock
    private File file;

  @Test
  public void getNamesTest() throws FileNotFoundException{


    when(webDriverFactory.newScanner(any())).thenReturn(scanner);
    when(scanner.nextLine()).thenReturn("uno").thenReturn("dos");
    when(webDriverFactory.newFile(anyString())).thenReturn(file);
    when(scanner.hasNextLine()).thenReturn(true).thenReturn(true).thenReturn(false);

    List<String> output = ioService.getNames("dummyFile.txt");

    assertThat(output.get(0)).isEqualTo("uno");
    assertThat(output.get(1)).isEqualTo("dos");


  }

}
