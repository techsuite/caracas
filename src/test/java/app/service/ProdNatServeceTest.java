package app.service;


import app.dao.ProdNatDAO;
import app.model.EnfermedadesModel;
import app.model.ProdNatModel;
import app.webscraping.pages.GenericPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProdNatServeceTest {

    @Autowired
    private ProdNatService prodNatService;

    @MockBean
    private GenericPage page;

    @MockBean
    private ProdNatDAO prodNatDAO;

    @Test
    public void actualizarProdNat_whenCalled_shouldExecute()
            throws Exception {
        ProdNatModel prodSp1 = new ProdNatModel("1", "spanish");
        ProdNatModel prodSp2 = new ProdNatModel("2", "spanish");
        ProdNatModel prodEn1= new ProdNatModel("1", "english");
        ProdNatModel prodEn2= new ProdNatModel("2", "english");
        List<ProdNatModel> dummyProdSp = List.of(prodSp1,prodSp2);
        List<ProdNatModel> dummyProdEn = List.of(prodEn1,prodEn2);

        when(page.getHerbsList(eq("spanish"), any())).thenReturn(dummyProdSp);
        when(page.getHerbsList(eq("english"), any())).thenReturn(dummyProdEn);

        prodNatService.actualizarProdNat();

        verify(prodNatDAO).insertarProdNat(prodSp1);
        verify(prodNatDAO).insertarProdNat(prodSp2);
        verify(prodNatDAO).insertarProdNat(prodEn1);
        verify(prodNatDAO).insertarProdNat(prodEn2);
    }


    @Test
    public void actualizarTodosProdNatDB_whenCalled_shouldExecute()
            throws Exception {
        ProdNatModel prodSp1 = new ProdNatModel("1", "spanish");
        ProdNatModel prodSp2 = new ProdNatModel("2", "spanish");
        ProdNatModel prodEn1= new ProdNatModel("1", "english");
        ProdNatModel prodEn2= new ProdNatModel("2", "english");
        List<ProdNatModel> dummyProdSp = List.of(prodSp1,prodSp2);
        List<ProdNatModel> dummyProdEn = List.of(prodEn1,prodEn2);

        when(page.getHerbsList(eq("spanish"), any())).thenReturn(dummyProdSp);
        when(page.getHerbsList(eq("english"), any())).thenReturn(dummyProdEn);

        prodNatService.actualizarTodosProdNatDB();

        verify(prodNatDAO).insertarProdNat(prodSp1);
        verify(prodNatDAO).insertarProdNat(prodSp2);
        verify(prodNatDAO).insertarProdNat(prodEn1);
        verify(prodNatDAO).insertarProdNat(prodEn2);
    }

    @Test
    public void actualizarElem_whenCalled_shouldExecute()
            throws Exception {
        ProdNatModel prodSp1 = new ProdNatModel("1", "spanish");
        Timestamp time = new Timestamp(System.currentTimeMillis());
        prodSp1.setFrecUpdateHours(0);
        prodSp1.setDateNextUpdate(time);

        List<ProdNatModel> dummyProdSp = List.of(prodSp1);

        when(page.getHerbsList(eq("spanish"), any())).thenReturn(dummyProdSp);

        prodNatService.actualizarElem("1","spanish",0, time);

        verify(prodNatDAO).insertarProdNat(prodSp1);
    }

}
