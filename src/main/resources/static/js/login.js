async function loginData() {
  const username = document.getElementById("nameInput").value;
  const password = document.getElementById("passwordInput").value;

  document.getElementById("ErrorText").style.display = "block";

  const response = await fetch("/login", {
    method: "POST",
    body: JSON.stringify({ username, password }),
    headers: {
      "Content-Type": "application/json",
    },
  });

  if (response.status === 200) {
    document.getElementById("logBox").style.display = "block";
    document.getElementById("ErrorText").innerHTML = "";
  } else {
    document.getElementById("ErrorText").innerHTML =
      "Usuario o contrasena incorrectas";
  }
}
