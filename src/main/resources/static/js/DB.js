
function busqueda() {
    var tableReg = document.getElementById('tabla1');
    var searchText = document.getElementById('buscador').value.toLowerCase();
    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        var found = false;
        for (var j = 0; j < cellsOfRow.length && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }
}

function upload(nombre, idioma){
    
    document.getElementById("textoEdit").innerHTML ="Inserte la nueva frecuencia de actualización de: " + nombre;

    if(document.getElementById("selEdBox").style.display == "none"){
        document.getElementById("selEdBox").style.display = "block";
    }else{
        document.getElementById("selEdBox").style.display = "none";
    }


    document.getElementById("nameEditAux").innerHTML = nombre;
    document.getElementById("languageEditAux").innerHTML = idioma;

}


function borrar(nombre, idioma, index, tipo){

    document.getElementById("eraseText").innerHTML = "¿Seguro que desea borrar " + nombre +" de la base de datos?";  
    document.getElementById("eraseBox").style.display = "block";

    document.getElementById("nameEraseAux").innerHTML= nombre;
    document.getElementById("languageEraseAux").innerHTML= idioma;
    document.getElementById("indexEraseAux").innerHTML= index;
    document.getElementById("typeEraseAux").innerHTML= tipo;
    
}

function confirmErase(){

    var nombre = document.getElementById("nameEraseAux").innerHTML;
    var idioma = document.getElementById("languageEraseAux").innerHTML;
    var index = document.getElementById("indexEraseAux").innerHTML;
    var tipo = document.getElementById("typeEraseAux").innerHTML;


    var url = 'https://caracas.juliocastrodev.duckdns.org/borrar' +tipo+'?nombre=' + nombre + '&idioma=' + idioma;

    document.getElementById("textoConfirma").innerHTML = url;
    var tableReg = document.getElementById('tabla1');
    tableReg.rows[index].style.display = 'none';
    fetch(url , {method: 'DELETE'});
    document.getElementById("eraseBox").style.display = "none";

}

function cancelErase(){

    document.getElementById("eraseBox").style.display = "none";

}

function sendEdit(tipo){

      var nombre = document.getElementById("nameEditAux").innerHTML;
      var lenguaje = document.getElementById("languageEditAux").innerHTML;
      var horas = document.getElementById("hoursInput").value;

      var url = 'https://caracas.juliocastrodev.duckdns.org/editar' +tipo+'?nombre=' + nombre + '&idioma=' + lenguaje + '&horas=' + horas;
      document.getElementById("textoConfirma").innerHTML = url;
 
      fetch(url , {method: 'POST'});
      document.getElementById("editBox1").style.display ="none";
}

function closeEdit(){

    document.getElementById("editBox1").style.display ="none";
}

function actualizar(nombre, idioma,frecUpdateHours, dateNextUpdate , tipo){

    document.getElementById("actualizarBox").style.display ="block";
    document.getElementById("actualizarName").innerHTML = "Se est&aacute actualizando: " + nombre;

    var url = 'https://caracas.juliocastrodev.duckdns.org/actualizar' +tipo+'?nombre=' + nombre + '&idioma=' + idioma + '&horas=' + frecUpdateHours + '&fecha='+dateNextUpdate;
    document.getElementById("textoConfirma").innerHTML = url;
    fetch(url , {method: 'POST'});

}

function removeActBox(){

    document.getElementById("actualizarBox").style.display ="none";

}

function showFrec(){

    document.getElementById("editBox1").style.display = "block";
    document.getElementById("selEdBox").style.display = "none";

}

function showDate(){

    var name = document.getElementById("nameEditAux").innerHTML

    document.getElementById("dateText").innerHTML = "Inserte la fecha de actualizaci&oacute;n de "+ name;
    document.getElementById("dateBox").style.display = "block";
    document.getElementById("selEdBox").style.display = "none";

}

function removeDateBox(){

    document.getElementById("dateBox").style.display ="none";

}

function sendDate(tipo){

    document.getElementById("dateBox").style.display ="none";

    var fecha = document.getElementById("dateInput").value;
    var nombre = document.getElementById("nameEditAux").innerHTML;
    var idioma = document.getElementById("languageEditAux").innerHTML;

    var url = 'https://caracas.juliocastrodev.duckdns.org/actualizarFecha' +tipo+'?nombre=' + nombre + '&idioma=' + idioma + '&fecha=' + fecha + " 00:00:00";
    fetch(url , {method: 'POST'});

    document.getElementById("dateBox").style.display ="none";
}