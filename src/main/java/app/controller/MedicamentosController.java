package app.controller;

import app.dao.MedicamentosDAO;
import app.factory.MedicamentosResponseFactory;
import app.model.response.ListMedicamentosResponse;
import app.model.response.MedicamentoResponse;
import app.model.response.Metadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class MedicamentosController{

  @Autowired
  private MedicamentosResponseFactory medicamentosResponseFactory;

  @Autowired
  private MedicamentosDAO medicamentosDAO;

  @GetMapping("/medicamentos")
  public ResponseEntity<ListMedicamentosResponse> medicamentos(
    @Valid Metadata metadata,
    @RequestParam String idioma
  ) throws Exception {
    Boolean result =
      metadata.getPageNumber() > 0 &&
      metadata.getPageSize() > 0 &&
      (idioma.equals("spanish") || idioma.equals("english"));
    List<MedicamentoResponse> res = null;

    if (result) {
      res =
        medicamentosDAO.getPagesWithIdioma(
          metadata,
          idioma,
          "medicamentos",
          MedicamentoResponse.class
        );
      Integer totalRows = medicamentosDAO.numOfRows("medicamentos", idioma);
      double div = Math.ceil((double) totalRows / metadata.getPageSize());
      metadata.setTotalPages((int) div);
    }
    ListMedicamentosResponse medicamentosResponse = medicamentosResponseFactory.newMedicamentosResponse(
      res,
      metadata
    );

    result = result && metadata.getPageNumber() <= metadata.getTotalPages();

    return result
      ? ResponseEntity.ok().body(medicamentosResponse)
      : ResponseEntity.badRequest().build();
  }
}
