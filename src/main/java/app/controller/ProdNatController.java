package app.controller;

import app.dao.ProdNatDAO;
import app.factory.ProdNatResponseFactory;
import app.model.response.ListProdNatsResponse;
import app.model.response.Metadata;
import app.model.response.ProdNatResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ProdNatController{

    @Autowired
    private ProdNatResponseFactory prodNatResponseFactory;

    @Autowired
    private ProdNatDAO prodNatDAO;



    @GetMapping("/prodNat")
    public ResponseEntity<ListProdNatsResponse> productos_naturales(@Valid Metadata metadata, @RequestParam String idioma) throws Exception {

        Boolean result = metadata.getPageNumber()>0 && metadata.getPageSize()>0 && (idioma.equals("spanish") || idioma.equals("english"));
        List<ProdNatResponse> res = null;

        if(result) {

            res = prodNatDAO.getPagesWithIdioma(metadata, idioma, "productos_naturales", ProdNatResponse.class);
            Integer totalRows = prodNatDAO.numOfRows("productos_naturales", idioma);
            double div = Math.ceil((double) totalRows / metadata.getPageSize());
            metadata.setTotalPages((int) div);
        }
        ListProdNatsResponse prodNatResponse = prodNatResponseFactory.newProdNatResponse(res, metadata);

        result = result && metadata.getPageNumber()<=metadata.getTotalPages();

        return result
                ?ResponseEntity.ok().body(prodNatResponse)
                :ResponseEntity.badRequest().build();
    }
}