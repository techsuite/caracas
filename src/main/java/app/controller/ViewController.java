package app.controller;

import app.dao.EnfermedadesDAO;
import app.dao.MedicamentosDAO;
import app.dao.ProdNatDAO;
import app.dataHolder.AuthDataHolder;
import app.model.*;
import app.model.request.LoginRequest;
import app.model.response.Metadata;
import app.service.UserService;
import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class ViewController {

  @Autowired
  private MedicamentosDAO medicamentosDAO;

  @Autowired
  private EnfermedadesDAO enfermedadesDAO;

  @Autowired
  private ProdNatDAO prodNatDAO;

  @Autowired
  private AuthDataHolder authDataHolder;

  @Autowired
  private UserService userService;

  @GetMapping("/solicitud")
  public String solicitudGET(Model model) throws Exception {
    model.addAttribute("solicitud", new Solicitud());
    return "solicitud";
  }

  @PostMapping("/solicitud")
  public String solicitudPOST(@ModelAttribute Solicitud solicitudRequest)
    throws Exception {
    // Codigo de introducir en la base de datos la solicitud
    if (solicitudRequest.getTipo().equals("medicamento")) {
      MedicamentosModel medicamento = new MedicamentosModel();
      medicamento.setNombre(solicitudRequest.getNombre());
      medicamento.setIdioma(solicitudRequest.getIdioma());
      medicamento.setFrecUpdateHours(3);
      medicamento.setDateNextUpdate(new Timestamp(System.currentTimeMillis()));
      medicamentosDAO.insertarMedicamento(medicamento);
    } else if (solicitudRequest.getTipo().equals("enfermedad")) {
      EnfermedadesModel enfermedad = new EnfermedadesModel();
      enfermedad.setNombre(solicitudRequest.getNombre());
      enfermedad.setIdioma(solicitudRequest.getIdioma());
      enfermedad.setFrecUpdateHours(3);
      enfermedad.setDateNextUpdate(new Timestamp(System.currentTimeMillis()));
      enfermedadesDAO.insertarEnfermedad(enfermedad);
    } else {
      ProdNatModel prodNat = new ProdNatModel();
      prodNat.setNombre(solicitudRequest.getNombre());
      prodNat.setIdioma(solicitudRequest.getIdioma());
      prodNat.setFrecUpdateHours(3);
      prodNat.setDateNextUpdate(new Timestamp(System.currentTimeMillis()));
      prodNatDAO.insertarProdNat(prodNat);
    }

    System.out.println(solicitudRequest);
    return "resultadoSolicitud";
  }

  @GetMapping("/medicamentosDB")
  public String medicamentosDB(Model model) throws Exception {
    Metadata metadataEsp = new Metadata(
      1,
      medicamentosDAO.numOfRows("medicamentos", "spanish"),
      1
    );
    List<MedicamentosModel> medicamentosEsp = medicamentosDAO.getPagesWithIdioma(
      metadataEsp,
      "spanish",
      "medicamentos",
      MedicamentosModel.class
    );
    Metadata metadataEng = new Metadata(
      1,
      medicamentosDAO.numOfRows("medicamentos", "english"),
      1
    );
    List<MedicamentosModel> medicamentos = medicamentosDAO.getPagesWithIdioma(
      metadataEng,
      "english",
      "medicamentos",
      MedicamentosModel.class
    );
    medicamentos.addAll(medicamentosEsp);

    model.addAttribute("medicamentos", medicamentos);

    return "medicamentosDB";
  }

  @GetMapping("/prodNatDB")
  public String prodNatDB(Model model) throws Exception {
    Metadata metadataEsp = new Metadata(
      1,
      prodNatDAO.numOfRows("productos_naturales", "spanish"),
      1
    );
    List<ProdNatModel> prodNatEsp = prodNatDAO.getPagesWithIdioma(
      metadataEsp,
      "spanish",
      "productos_naturales",
      ProdNatModel.class
    );
    Metadata metadataEng = new Metadata(
      1,
      prodNatDAO.numOfRows("productos_naturales", "english"),
      1
    );
    List<ProdNatModel> prodNat = prodNatDAO.getPagesWithIdioma(
      metadataEng,
      "english",
      "productos_naturales",
      ProdNatModel.class
    );
    prodNat.addAll(prodNatEsp);

    model.addAttribute("prodNat", prodNat);

    return "prodNatDB";
  }

  @GetMapping("/enfermedadesDB")
  public String enfermedadesDB(Model model) throws Exception {
    Metadata metadataEsp = new Metadata(
      1,
      medicamentosDAO.numOfRows("enfermedades", "spanish"),
      1
    );
    List<EnfermedadesModel> enfermedadesEsp = medicamentosDAO.getPagesWithIdioma(
      metadataEsp,
      "spanish",
      "enfermedades",
      EnfermedadesModel.class
    );
    Metadata metadataEng = new Metadata(
      1,
      medicamentosDAO.numOfRows("enfermedades", "english"),
      1
    );
    List<EnfermedadesModel> enfermedades = medicamentosDAO.getPagesWithIdioma(
      metadataEng,
      "english",
      "enfermedades",
      EnfermedadesModel.class
    );
    enfermedades.addAll(enfermedadesEsp);

    model.addAttribute("enfermedades", enfermedades);

    return "enfermedadesDB";
  }

  @GetMapping("/administracion")
  public String admin() {
    return "administracion";
  }

  @GetMapping("/index")
  public String index() {
    return "index";
  }

  @GetMapping("/login")
  public String login() {
    this.authDataHolder.setLoggedIn(false); // close user session
    return "login";
  }

  @PostMapping("/login")
  @ResponseBody
  public ResponseEntity<Void> doLogin(@RequestBody LoginRequest loginRequest) {
    boolean areCredentialsCorrect = userService.auth(
      loginRequest.getUsername(),
      loginRequest.getPassword()
    );
    this.authDataHolder.setLoggedIn(areCredentialsCorrect);

    return areCredentialsCorrect
      ? ResponseEntity.ok().build()
      : ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
  }
}
