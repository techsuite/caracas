package app.controller;

import app.dao.EnfermedadesDAO;
import app.factory.EnfermedadesResponseFactory;
import app.model.response.EnfermedadResponse;
import app.model.response.ListEnfermedadesResponse;
import app.model.response.Metadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class EnfermedadesController {

    @Autowired
    private EnfermedadesResponseFactory enfermedadesResponseFactory;

    @Autowired
    private EnfermedadesDAO enfermedadesDAO;


    @GetMapping("/enfermedades")
    public ResponseEntity<ListEnfermedadesResponse> enfermedades(@Valid Metadata metadata, @RequestParam String idioma) throws Exception {

        Boolean result = metadata.getPageNumber() > 0 && metadata.getPageSize() > 0 && (idioma.equals("spanish") || idioma.equals("english"));
        List<EnfermedadResponse> res = null;

        if (result) {

            res = enfermedadesDAO.getPagesWithIdioma(metadata, idioma, "enfermedades", EnfermedadResponse.class);
            Integer totalRows = enfermedadesDAO.numOfRows("enfermedades", idioma);
            double div = Math.ceil((double) totalRows / metadata.getPageSize());
            metadata.setTotalPages((int) div);
        }
        ListEnfermedadesResponse enfermedadesResponse = enfermedadesResponseFactory.newEnfermedadesResponse(res, metadata);

        result = result && metadata.getPageNumber() <= metadata.getTotalPages();

        return result
                ? ResponseEntity.ok().body(enfermedadesResponse)
                : ResponseEntity.badRequest().build();
    }
}