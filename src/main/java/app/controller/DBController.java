package app.controller;


import app.dao.EnfermedadesDAO;
import app.dao.GenericDAO;
import app.dao.MedicamentosDAO;
import app.dao.ProdNatDAO;
import app.factory.EnfermedadesResponseFactory;
import app.model.MedicamentosModel;
import app.model.ProdNatModel;
import app.model.response.EnfermedadResponse;
import app.model.response.ListEnfermedadesResponse;
import app.model.response.ListMedicamentosResponse;
import app.model.response.Metadata;
import app.service.ChronoService;
import app.service.EnfermedadesService;
import app.service.MedicamentosService;
import app.service.ProdNatService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.sql.Timestamp;
import java.util.List;
@RestController
public class DBController {

    @Autowired
    private EnfermedadesDAO enfermedadesDAO;

    @Autowired
    private MedicamentosDAO medicamentosDAO;

    @Autowired
    private ProdNatDAO prodNatDAO;

    @Autowired
    private ProdNatService prodNatService;

    @Autowired
    private MedicamentosService medicamentosService;

    @Autowired
    private EnfermedadesService enfermedadesService;
    
    @Autowired
    private ChronoService chronoService;

    @DeleteMapping("/borrarMedicamento")
    public void borrarMedicamento(@RequestParam String nombre,@RequestParam String idioma) throws  Exception{
        medicamentosDAO.deleteFromQuery("medicamentos",nombre,idioma);
    }

    @DeleteMapping("/borrarEnfermedad")
    public void borrarEnfermedad(@RequestParam String nombre,@RequestParam String idioma) throws  Exception{
        enfermedadesDAO.deleteFromQuery("enfermedades",nombre,idioma);
    }

    @DeleteMapping("/borrarProdNat")
    public void borrarProdNat(@RequestParam String nombre,@RequestParam String idioma) throws  Exception{
        prodNatDAO.deleteFromQuery("productos_naturales",nombre,idioma);
    }

    @PostMapping("/editarMedicamento")
    public void editarMedicamento(@RequestParam String nombre,@RequestParam String idioma,@RequestParam String horas) throws  Exception{
        medicamentosDAO.updateFrecUpdate(nombre, idioma, Integer.parseInt(horas));
    }

    @PostMapping("/editarEnfermedad")
    public void editarEnfermedad(@RequestParam String nombre,@RequestParam String idioma,@RequestParam String horas) throws  Exception{
        enfermedadesDAO.updateFrecUpdate(nombre, idioma, Integer.parseInt(horas));
    }

    @PostMapping("/editarProdNat")
    public void editarFreqProdNat(@RequestParam String nombre,@RequestParam String idioma,@RequestParam String horas) throws  Exception{
        prodNatDAO.updateFrecUpdate(nombre, idioma, Integer.parseInt(horas));
    }

    @PostMapping("/actualizarProdNat")
    public void actualizarProdNat(@RequestParam String nombre,@RequestParam String idioma,@RequestParam String horas ,@RequestParam String fecha) throws  Exception{
        prodNatService.actualizarElem(nombre, idioma, Integer.parseInt(horas), Timestamp.valueOf(fecha));
    }

    @PostMapping("/actualizarMedicamento")
    public void actualizarMedicamento(@RequestParam String nombre,@RequestParam String idioma,@RequestParam String horas ,@RequestParam String fecha) throws  Exception{
        medicamentosService.actualizarElem(nombre, idioma, Integer.parseInt(horas), Timestamp.valueOf(fecha));
    }

    @PostMapping("/actualizarEnfermedad")
    public void actualizarEnfermedad(@RequestParam String nombre,@RequestParam String idioma,@RequestParam String horas ,@RequestParam String fecha) throws  Exception{
        enfermedadesService.actualizarElem(nombre, idioma, Integer.parseInt(horas), Timestamp.valueOf(fecha));
    }

    @PostMapping("/actualizarTodo")
    public void actualizartodo() throws  Exception{
        chronoService.actualizarDB();
    }

    @PostMapping("/actualizarTodo/{category}")
    public void actualizarTodoDeUnaCategoria(@PathVariable String category) throws Exception {
       switch (category) {
           case "medicamentos":
               medicamentosService.actualizarMedicamentos();
               break;

           case "enfermedades":
               enfermedadesService.actualizarEnfermedades();
               break;

           case "productos_naturales":
               prodNatService.actualizarProdNat();
               break;
       }
    }

    @PostMapping("/actualizarFechaProdNat")
    public void actualizarFechaProdNat(@RequestParam String nombre,@RequestParam String idioma ,@RequestParam String fecha) throws  Exception{
        prodNatDAO.updatedateNextUpdate(nombre, idioma, Timestamp.valueOf(fecha));
    }

    @PostMapping("/actualizarFechaMedicamento")
    public void actualizarFechaMedicamento(@RequestParam String nombre,@RequestParam String idioma ,@RequestParam String fecha) throws  Exception{
        medicamentosDAO.updatedateNextUpdate(nombre, idioma, Timestamp.valueOf(fecha));
    }

    @PostMapping("/actualizarFechaEnfermedad")
    public void actualizarFechaEnfermedad(@RequestParam String nombre,@RequestParam String idioma ,@RequestParam String fecha) throws  Exception{
        enfermedadesDAO.updatedateNextUpdate(nombre, idioma, Timestamp.valueOf(fecha));
    }


}
