package app.interceptor;

import app.dataHolder.AuthDataHolder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.hc.core5.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class LoginInterceptor implements HandlerInterceptor {

  @Autowired
  private AuthDataHolder authDataHolder;

  @Override
  public boolean preHandle(
    HttpServletRequest request,
    HttpServletResponse response,
    Object handler
  ) {
    if (authDataHolder.isLoggedIn()) {
      return true; // continue
    }

    // else redirect to login
    String encodedRedirectURL = response.encodeRedirectURL("/login");
    response.setStatus(HttpStatus.SC_TEMPORARY_REDIRECT);
    response.setHeader("Location", encodedRedirectURL);
    return false;
  }
}
