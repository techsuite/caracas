package app.service;


import app.dao.ProdNatDAO;
import app.model.EnfermedadesModel;
import app.model.MedicamentosModel;
import app.model.ProdNatModel;
import app.webscraping.pages.GenericPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

@Service
public class ProdNatService {

    @Autowired
    private GenericPage page;

    @Autowired
    private ProdNatDAO prodNatDAO;

    //tarea que actualiza perdicamente la tabla de prodNat
    public void actualizarProdNat() throws Exception {

        //HAY QUE SACAR LOS DATOS DE LA DB -> fechaActualizar <= fechaNow

        System.out.println("Comienza ProdNat");

        //lista de med en espanol

        List<ProdNatModel> prodSp = prodNatDAO.prodNatQueActualizarSp();

        prodSp= page.getHerbsList("spanish", prodSp);

        int numSp = prodSp.size();

        //lista de med en ingles
        List<ProdNatModel> prodEn = prodNatDAO.prodNatQueActualizarEn();

        prodEn = page.getHerbsList("english", prodEn);
        int numEn = prodEn.size();

        //anadimos los medicamentos en espanol
        for (int i = 0; i<numSp; i++){
            prodNatDAO.insertarProdNat(prodSp.get(i));
        }

        //anadimos los medicamentos en ingles
        for (int i = 0; i<numEn; i++){
            prodNatDAO.insertarProdNat(prodEn.get(i));
        }
        /*
        ProdNatModel med = new ProdNatModel("prod prueba","spanish");
        med.setNextUpdate(new Timestamp(121,4,7,6,7,1,3));
        prodNatDAO.insertarProdNat(med);*/
        System.out.println("Fin ProdNat");
    }


    public void actualizarTodosProdNatDB()throws Exception{

        //productos naturales de la base de datos en espanol

        List<ProdNatModel> prodDBSp = prodNatDAO.getProdNatFromDBSp();
        //hago webscraping
        prodDBSp= page.getHerbsList("spanish", prodDBSp);
        int numSp = prodDBSp.size();

        //productos naturales de la base de datos en ingles

        List<ProdNatModel> prodDBEn = prodNatDAO.getProdNatFromDBEn();
        //hago webscraping
        prodDBEn= page.getHerbsList("english", prodDBEn);
        int numEn = prodDBEn.size();

        //introducimos en la base de satos

        //espanol
        for (int i = 0; i<numSp; i++){
            prodNatDAO.insertarProdNat(prodDBSp.get(i));
        }

        //ingles
        for (int i = 0; i<numEn; i++){
            prodNatDAO.insertarProdNat(prodDBEn.get(i));
        }
    }

    public void actualizarElem(String name, String idioma, int frec, Timestamp time) throws Exception{
        //creo el objeto
        ProdNatModel prod = new ProdNatModel(name, idioma);
        prod.setFrecUpdateHours(frec);
        prod.setDateNextUpdate(time);

        List<ProdNatModel> prodActualizar = List.of(prod);

        //hago el webscarping del elemento
        prodActualizar=page.getHerbsList(idioma, prodActualizar);
        //inserto en la base de datos
        prodNatDAO.insertarProdNat(prodActualizar.get(0));
    }

}
