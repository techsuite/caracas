package app.service;

import app.factory.WebDriverFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

@Service
public class IOService {

  @Autowired
  private WebDriverFactory webDriverFactory;

  public List<String> getNames(String filename) throws FileNotFoundException {
    Scanner scanner =  webDriverFactory.newScanner(webDriverFactory.newFile(filename));

    LinkedList<String> names = new LinkedList<>();

    while (scanner.hasNextLine()) {
      names.addLast(scanner.nextLine());
    }

    scanner.close();

    return names;

  }
}
