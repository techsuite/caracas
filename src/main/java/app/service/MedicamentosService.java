package app.service;


import app.dao.MedicamentosDAO;
import app.model.EnfermedadesModel;
import app.model.MedicamentosModel;
import app.model.ProdNatModel;
import app.webscraping.pages.GenericPage;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

@Service
public class MedicamentosService {

  @Autowired
  private GenericPage page;

  @Autowired
  private MedicamentosDAO medicamentosDAO;

  //tarea que actualiza perdicamente la tabla de medicamentos
  public void actualizarMedicamentos() throws Exception {

    //HAY QUE SACAR LOS DATOS DE LA DB -> fechaActualizar <= fechaNow

    System.out.println("Comienza a actualizar medicamentos");
    //lista de med en espanol
    List<MedicamentosModel> drugsSp = medicamentosDAO.medQueActualizarSp();

    drugsSp = page.getDrugsList("spanish", drugsSp);
    int numSp = drugsSp.size();

    //lista de med en ingles
    List<MedicamentosModel> drugsEn = medicamentosDAO.medQueActualizarEn();

    drugsEn = page.getDrugsList("english", drugsEn);
    int numEn = drugsEn.size();

    //anadimos los medicamentos en espanol
    for (int i = 0; i<numSp; i++){
      medicamentosDAO.insertarMedicamento(drugsSp.get(i));
    }

    //anadimos los medicamentos en ingles
    for (int i = 0; i<numEn; i++){
      medicamentosDAO.insertarMedicamento(drugsEn.get(i));
    }
    /*
    MedicamentosModel med = new MedicamentosModel("med prueba","spanish");
    med.setNextUpdate(new Timestamp(121,4,7,6,7,1,3));
    medicamentosDAO.insertarMedicamento(med);*/
    System.out.println("Medicamentos actualizados");

  }


  public void actualizarTodosMedicamentosDB()throws Exception{

    //medicamentos de la base de datos en espanol

    System.out.println("Que vaaaaaa");
    List<MedicamentosModel> medicamentosDBSp = medicamentosDAO.getMedicamentosFromDBSp();
    //hago webscraping
    medicamentosDBSp= page.getDrugsList("spanish", medicamentosDBSp);
    int numSp = medicamentosDBSp.size();

    //medicamentos de la base de datos en ingles

    List<MedicamentosModel> medicamentosDBEn = medicamentosDAO.getMedicamentosFromDBEn();
    //hago webscraping
    medicamentosDBEn= page.getDrugsList("english", medicamentosDBEn);
    int numEn = medicamentosDBEn.size();

    //introducimos en la base de satos

    //espanol
    for (int i = 0; i<numSp; i++){
      medicamentosDAO.insertarMedicamento(medicamentosDBSp.get(i));
    }

    //ingles
    for (int i = 0; i<numEn; i++){
      medicamentosDAO.insertarMedicamento(medicamentosDBEn.get(i));
    }

    System.out.println("Po ya ta");
  }

  public void actualizarElem(String name, String idioma, int frec, Timestamp time) throws Exception{
    //creo el objeto
    MedicamentosModel med = new MedicamentosModel(name, idioma);
    med.setFrecUpdateHours(frec);
    med.setDateNextUpdate(time);

    List<MedicamentosModel> medActualizar = List.of(med);

    //hago el webscarping del elemento
    medActualizar=page.getDrugsList(idioma, medActualizar);
    //inserto en la base de datos
    medicamentosDAO.insertarMedicamento(medActualizar.get(0));
  }
}
