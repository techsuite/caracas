package app.service;

import app.dao.EnfermedadesDAO;
import app.dao.MedicamentosDAO;
import app.model.MedicamentosModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;


@RestController
public class ChronoService {

    @Autowired
    private MedicamentosService medicamentosService;

    @Autowired
    private EnfermedadesService enfermedadesService;

    @Autowired
    private ProdNatService prodNatService;

    @Autowired
    private MedicamentosDAO medicamentosDAO;

    //Busca cada 12h los elementos que se tienen que actualizar
    @Scheduled(/*fixedRate = 400000)*/cron = "0 0 */24 * * *")
    public void actualizarDB() throws Exception {

        medicamentosService.actualizarMedicamentos();

        enfermedadesService.actualizarEnfermedades();

        prodNatService.actualizarProdNat();

    }


    //actualiza toda la base de datos
    public void updateAllDB() throws Exception{

        medicamentosService.actualizarTodosMedicamentosDB();

        enfermedadesService.actualizarTodasEnfermedadesDB();

        prodNatService.actualizarTodosProdNatDB();
    }
}
