package app.service;

import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  private static final Map<String, String> USERS = Map.of(
    "pablo",
    "1234",
    "maria",
    "abcd",
    "alfonso",
    "aaaa"
  );

  public boolean auth(String user, String password) {
    if (!USERS.containsKey(user)) { // user not found
      return false;
    }

    return USERS.get(user).equals(password);
  }
}
