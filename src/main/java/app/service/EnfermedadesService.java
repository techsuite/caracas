package app.service;


import app.dao.EnfermedadesDAO;
import app.model.EnfermedadesModel;
import app.model.MedicamentosModel;
import app.webscraping.pages.GenericPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

@Service
public class EnfermedadesService {
    @Autowired
    private GenericPage page;

    @Autowired
    private EnfermedadesDAO enfermedadesDAO;


    //tarea que actualiza perdicamente la tabla de enfermedades
    public void actualizarEnfermedades() throws Exception {

        //HAY QUE SACAR LOS DATOS DE LA DB -> fechaActualizar <= fechaNow

        List<EnfermedadesModel> enfSp = enfermedadesDAO.enfermedadesQueActualizarSp();

        //lista de med en espanol
        enfSp= page.getIllnessesList("spanish", enfSp);
        int numSp = enfSp.size();

        //lista de med en ingles
        List<EnfermedadesModel> enfEn = enfermedadesDAO.enfermedadesQueActualizarEn();
        enfEn=page.getIllnessesList("english", enfEn);
        int numEn = enfEn.size();

        //anadimos los enfermedades en espanol
       for (int i = 0; i<numSp; i++){
           enfermedadesDAO.insertarEnfermedad(enfSp.get(i));
           //System.out.println(enfSp.get(i));
        }

        //anadimos los enfermedades en ingles
       for (int i = 0; i<numEn; i++){
           enfermedadesDAO.insertarEnfermedad(enfEn.get(i));
        }
        /*
        EnfermedadesModel enf = new EnfermedadesModel("enf prueba","spanish");
        enf.setNextUpdate(new Timestamp(121,4,7,6,7,1,3));
        enfermedadesDAO.insertarEnfermedad(enf);
        */


        System.out.println("Fin Enfermedades");
    }

    public void actualizarTodasEnfermedadesDB()throws Exception{

        //enfermedades de la base de datos en espanol

        List<EnfermedadesModel> enfermedadesDBSp = enfermedadesDAO.getEnfermedadesFromDBSp();
        //hago webscraping
        enfermedadesDBSp= page.getIllnessesList("spanish", enfermedadesDBSp);
        int numSp = enfermedadesDBSp.size();

        //enfermedades de la base de datos en ingles

        List<EnfermedadesModel> enfermedadesDBEn = enfermedadesDAO.getEnfermedadesFromDBEn();
        //hago webscraping
        enfermedadesDBEn= page.getIllnessesList("english", enfermedadesDBEn);
        int numEn = enfermedadesDBEn.size();

        //introducimos en la base de satos

        //espanol
        for (int i = 0; i<numSp; i++){
            enfermedadesDAO.insertarEnfermedad(enfermedadesDBSp.get(i));
        }

        //ingles
        for (int i = 0; i<numEn; i++){
            enfermedadesDAO.insertarEnfermedad(enfermedadesDBEn.get(i));
        }
    }

    public void actualizarElem(String name, String idioma, int frec, Timestamp time) throws Exception{
        //creo el objeto
        EnfermedadesModel enf = new EnfermedadesModel(name, idioma);
        enf.setFrecUpdateHours(frec);
        enf.setDateNextUpdate(time);

        List<EnfermedadesModel> enfActualizar = List.of(enf);

        //hago el webscarping del elemento
        enfActualizar=page.getIllnessesList(idioma, enfActualizar);
        //inserto en la base de datos
        enfermedadesDAO.insertarEnfermedad(enfActualizar.get(0));
    }

}
