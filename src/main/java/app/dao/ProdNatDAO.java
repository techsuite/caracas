package app.dao;


import app.dao.connector.Connector;
import app.model.EnfermedadesModel;
import app.model.ProdNatModel;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import java.sql.Timestamp;


@Repository
public class ProdNatDAO extends GenericDAO{

    @Autowired
    private Connector connector;

    @Autowired
    private QueryRunner queryRunner;

    /**
     * Inserta en la tabla y en caso de existir actualiza
     * @param prodNat
     * @throws SQLException
     **/

    public void insertarProdNat(ProdNatModel prodNat) throws SQLException {
        Timestamp actual = new Timestamp(System.currentTimeMillis());
        //sentencia sql insertar
        String sql = "INSERT INTO `caracas`.`productos_naturales` (nombre,contraind, posologia, prospecto, composicion, url, idioma, frecUpdateHours, dateNextUpdate) "+
                "VALUES (?,?, ?, ?, ?, ?, ?, ?, FROM_UNIXTIME(?)) "+
                "ON DUPLICATE KEY UPDATE "+
                "contraind= ?,"+
                "posologia= ?,"+
                "prospecto = ?,"+
                "composicion = ?,"+
                "url = ?,"+
                "frecUpdateHours = ?,"+
                "dateNextUpdate = FROM_UNIXTIME(?)";
        //insertamos en la base de datos con queryRunner
        try (Connection conn = connector.getConnection()) {
            queryRunner.insert(
                    conn,
                    sql,
                    new ScalarHandler<>(),
                    prodNat.getNombre(), prodNat.getContraind(),prodNat.getPosologia(), prodNat.getProspecto(),
                    prodNat.getComposicion(), prodNat.getUrl(), String.valueOf(prodNat.getIdioma()),
                    prodNat.getFrecUpdateHours(),prodNat.getDateNextUpdate().toInstant().getEpochSecond(),
                    prodNat.getContraind(),prodNat.getPosologia(), prodNat.getProspecto(),
                    prodNat.getComposicion(), prodNat.getUrl(),
                    //cambiamos la frecuencia a segundos y se la sumamos a la fecha actualizacion
                    //asi obtenemos nueva fecha de actualizacion
                    prodNat.getFrecUpdateHours(),
                    prodNat.getFrecUpdateHours()*3600*24+actual.toInstant().getEpochSecond());

        }
    }

    public List<ProdNatModel> prodNatQueActualizarSp() throws SQLException {
        List<ProdNatModel> prodQueActualizar = new LinkedList<>();

        String sql = "SELECT * FROM caracas.productos_naturales\n" +
                "where dateNextUpdate<=now() and idioma=\"spanish\";";

        try (Connection conn = connector.getConnection()) {
            prodQueActualizar =
                    queryRunner.query(conn, sql, new BeanListHandler<>(ProdNatModel.class));
        }

        return prodQueActualizar;
    }

    public List<ProdNatModel> prodNatQueActualizarEn() throws SQLException {
        List<ProdNatModel> prodQueActualizar = new LinkedList<>();

        String sql = "SELECT * FROM caracas.productos_naturales\n" +
                "where dateNextUpdate<=now() and idioma=\"english\";";

        try (Connection conn = connector.getConnection()) {
            prodQueActualizar =
                    queryRunner.query(conn, sql, new BeanListHandler<>(ProdNatModel.class));
        }

        return prodQueActualizar;
    }

    public List<ProdNatModel> getProdNatFromDBSp() throws SQLException {
        List<ProdNatModel> prodDB = new LinkedList<>();

        String sql = "SELECT * FROM caracas.productos_naturales\n" +
                "where idioma=\"spanish\";";

        try (Connection conn = connector.getConnection()) {
            prodDB =
                    queryRunner.query(conn, sql, new BeanListHandler<>(ProdNatModel.class));
        }

        return prodDB;
    }

    public List<ProdNatModel> getProdNatFromDBEn() throws SQLException {
        List<ProdNatModel> prodDB = new LinkedList<>();

        String sql = "SELECT * FROM caracas.productos_naturales\n" +
                "where idioma=\"english\";";

        try (Connection conn = connector.getConnection()) {
            prodDB =
                    queryRunner.query(conn, sql, new BeanListHandler<>(ProdNatModel.class));
        }

        return prodDB;
    }


    public void updateFrecUpdate(String name, String idioma, int frec) throws SQLException{

        String sql = "UPDATE `caracas`.`productos_naturales` "+
                "SET `frecUpdateHours` = ? WHERE (`nombre` = ?) and (`idioma` = ?);";

        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn,
                    sql,
                    frec,
                    name,
                    idioma);
        }
    }

    public void updatedateNextUpdate(String name, String idioma, Timestamp fecha) throws SQLException{

        String sql = "UPDATE `caracas`.`productos_naturales` "+
                "SET `dateNextUpdate` = ? WHERE (`nombre` = ?) and (`idioma` = ?);";

        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn,
                    sql,
                    fecha,
                    name,
                    idioma);
        }
    }

}
