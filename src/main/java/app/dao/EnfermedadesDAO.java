package app.dao;


import app.dao.connector.Connector;
import app.model.EnfermedadesModel;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

@Repository
public class EnfermedadesDAO extends GenericDAO{
    @Autowired
    private Connector connector;

    @Autowired
    private QueryRunner queryRunner;

    /**
     * Inserta en la tabla y en caso de existir actualiza
     * @param enfermedad
     * @throws SQLException
     */
    public void insertarEnfermedad(EnfermedadesModel enfermedad) throws SQLException {

        Timestamp actual = new Timestamp(System.currentTimeMillis());

        //sentencia sql insertar
        String sql = "INSERT INTO `caracas`.`enfermedades` (nombre,sintomas, recomendaciones, url, idioma, frecUpdateHours, dateNextUpdate) "+
                "VALUES ( ?, ?, ?, ?, ?, ?, FROM_UNIXTIME(?)) "+
                "ON DUPLICATE KEY UPDATE "+
                "sintomas= ?,"+
                "recomendaciones= ?,"+
                "url= ?,"+
                "frecUpdateHours = ?,"+
                "dateNextUpdate = FROM_UNIXTIME(?)";
        //insertamos en la base de datos con queryRunner
        try (Connection conn = connector.getConnection()) {
            queryRunner.insert(
                    conn,
                    sql,
                    new ScalarHandler<>(),
                    enfermedad.getNombre(), enfermedad.getSintomas(),
                    enfermedad.getRecomendaciones(), enfermedad.getUrl(), enfermedad.getIdioma(),

                    enfermedad.getFrecUpdateHours(), enfermedad.getDateNextUpdate().toInstant().getEpochSecond(),
                    enfermedad.getSintomas(),
                    enfermedad.getRecomendaciones(), enfermedad.getUrl(),
                    //cambiamos la frecuencia a segundos y se la sumamos a la fecha actualizacion
                    //asi obtenemos nueva fecha de actualizacion
                    enfermedad.getFrecUpdateHours(),
                    enfermedad.getFrecUpdateHours()*3600*24+actual.toInstant().getEpochSecond());

        }
    }


    public List<EnfermedadesModel> enfermedadesQueActualizarSp() throws SQLException {
        List<EnfermedadesModel> enfermedadesQueActualizar = new LinkedList<>();

        String sql = "SELECT * FROM caracas.enfermedades\n" +
                "where dateNextUpdate<=now() and idioma=\"spanish\";";

        try (Connection conn = connector.getConnection()) {
            enfermedadesQueActualizar =
                    queryRunner.query(conn, sql, new BeanListHandler<>(EnfermedadesModel.class));
        }

        return enfermedadesQueActualizar;
    }

    public List<EnfermedadesModel> enfermedadesQueActualizarEn() throws SQLException {
        List<EnfermedadesModel> enfermedadesQueActualizar = new LinkedList<>();

        String sql = "SELECT * FROM caracas.enfermedades\n" +
                "where dateNextUpdate<=now() and idioma=\"english\";";

        try (Connection conn = connector.getConnection()) {
            enfermedadesQueActualizar =
                    queryRunner.query(conn, sql, new BeanListHandler<>(EnfermedadesModel.class));
        }

        return enfermedadesQueActualizar;
    }

    public List<EnfermedadesModel> getEnfermedadesFromDBSp() throws SQLException{
        List<EnfermedadesModel> enfermedadesDB = new LinkedList<>();

        String sql = "SELECT * FROM caracas.enfermedades\n" +
                "where idioma=\"spanish\";";

        try (Connection conn = connector.getConnection()) {
            enfermedadesDB =
                    queryRunner.query(conn, sql, new BeanListHandler<>(EnfermedadesModel.class));
        }

        return enfermedadesDB;
    }

    public List<EnfermedadesModel> getEnfermedadesFromDBEn() throws SQLException{
        List<EnfermedadesModel> enfermedadesDB = new LinkedList<>();

        String sql = "SELECT * FROM caracas.enfermedades\n" +
                "where idioma=\"english\";";

        try (Connection conn = connector.getConnection()) {
            enfermedadesDB =
                    queryRunner.query(conn, sql, new BeanListHandler<>(EnfermedadesModel.class));
        }

        return enfermedadesDB;
    }

    public void updateFrecUpdate(String name, String idioma, int frec) throws SQLException{

        String sql = "UPDATE `caracas`.`enfermedades` "+
                "SET `frecUpdateHours` = ? WHERE (`nombre` = ?) and (`idioma` = ?);";

        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn,
                    sql,
                    frec,
                    name,
                   idioma);
        }
    }

    public void updatedateNextUpdate(String name, String idioma, Timestamp fecha) throws SQLException{

        String sql = "UPDATE `caracas`.`medicamentos` "+
                "SET `dateNextUpdate` = ? WHERE (`nombre` = ?) and (`idioma` = ?);";

        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn,
                    sql,
                    fecha,
                    name,
                    idioma);
        }
    }
}
