package app.dao;


import app.dao.connector.Connector;
import app.model.EnfermedadesModel;
import app.model.MedicamentosModel;
import app.model.ProdNatModel;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

@Repository
public class MedicamentosDAO extends GenericDAO {
//*************************************************************************************************
// CLASE PARA COMUNICARSE CON LA TABLA MEDICAMENTOS DE LA BBDD
// *****************************************************************************

    @Autowired
    private Connector connector;

    @Autowired
    private QueryRunner queryRunner;


    /**
     * Inserta en la tabla y en caso de existir actualiza
     * @param medicamento
     * @throws SQLException
     */
    public void insertarMedicamento(MedicamentosModel medicamento) throws SQLException {

        Timestamp actual = new Timestamp(System.currentTimeMillis());
        //sentencia sql insertar
        String sql = "" +
                "INSERT INTO `caracas`.`medicamentos` (nombre,contraind, posologia, prospecto, composicion, url, idioma, frecUpdateHours, dateNextUpdate)\n" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, FROM_UNIXTIME(?))" +
                "ON DUPLICATE KEY UPDATE " +
                "  contraind= ?," +
                "  posologia= ?, " +
                "  prospecto = ?," +
                "  composicion = ?," +
                "  url = ?," +
                "  frecUpdateHours = ?," +
                "  dateNextUpdate = FROM_UNIXTIME(?)";
        //insertamos en la base de datos con queryRunner
        try (Connection conn = connector.getConnection()) {
            queryRunner.insert(
                    conn,
                    sql,
                    new ScalarHandler<>(),
                    medicamento.getNombre(), medicamento.getContraind(), medicamento.getPosologia(), medicamento.getProspecto(),
                    medicamento.getComposicion(), medicamento.getUrl(), String.valueOf(medicamento.getIdioma()),

                    medicamento.getFrecUpdateHours(),medicamento.getDateNextUpdate().toInstant().getEpochSecond(),
                    medicamento.getContraind(),medicamento.getPosologia(), medicamento.getProspecto(),
                    medicamento.getComposicion(), medicamento.getUrl(),
                    //cambiamos la frecuencia a segundos y se la sumamos a la fecha actualizacion
                    //asi obtenemos nueva fecha de actualizacion
                    medicamento.getFrecUpdateHours(),
                    medicamento.getFrecUpdateHours()*3600*24+actual.toInstant().getEpochSecond());


        }

    }
    public List<MedicamentosModel> medQueActualizarSp() throws SQLException {
        List<MedicamentosModel> medQueActualizar = new LinkedList<>();

        String sql = "SELECT * FROM caracas.medicamentos\n" +
                    "where dateNextUpdate<=now() and idioma=\"spanish\";";

        try (Connection conn = connector.getConnection()) {
                medQueActualizar = queryRunner.query(conn, sql, new BeanListHandler<>(MedicamentosModel.class));
        }

        return medQueActualizar;
    }

    public List<MedicamentosModel> medQueActualizarEn() throws SQLException {
        List<MedicamentosModel> medQueActualizar = new LinkedList<>();

        String sql = "SELECT * FROM caracas.medicamentos\n" +
                    "where dateNextUpdate<=now() and idioma=\"english\";";

        try (Connection conn = connector.getConnection()) {
                medQueActualizar = queryRunner.query(conn, sql, new BeanListHandler<>(MedicamentosModel.class));
        }

        return medQueActualizar;
    }

    public List<MedicamentosModel> getMedicamentosFromDBSp() throws SQLException{
        List<MedicamentosModel> medicamentosDB = new LinkedList<>();

        String sql = "SELECT * FROM caracas.medicamentos\n" +
                "where idioma=\"spanish\";";

        try (Connection conn = connector.getConnection()) {
            medicamentosDB =
                    queryRunner.query(conn, sql, new BeanListHandler<>(MedicamentosModel.class));
        }

        return medicamentosDB;
    }

    public List<MedicamentosModel> getMedicamentosFromDBEn() throws SQLException{
        List<MedicamentosModel> medicamentosDB = new LinkedList<>();

        String sql = "SELECT * FROM caracas.medicamentos\n" +
                "where idioma=\"english\";";

        try (Connection conn = connector.getConnection()) {
            medicamentosDB =
                    queryRunner.query(conn, sql, new BeanListHandler<>(MedicamentosModel.class));
        }

        return medicamentosDB;
    }

    public void updateFrecUpdate(String name, String idioma, int frec) throws SQLException{

        String sql = "UPDATE `caracas`.`medicamentos` "+
                "SET `frecUpdateHours` = ? WHERE (`nombre` = ?) and (`idioma` = ?);";

        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn,
                    sql,
                    frec,
                    name,
                    idioma);
        }
    }

    public void updatedateNextUpdate(String name, String idioma, Timestamp fecha) throws SQLException{

        String sql = "UPDATE `caracas`.`medicamentos` "+
                "SET `dateNextUpdate` = ? WHERE (`nombre` = ?) and (`idioma` = ?);";

        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn,
                    sql,
                    fecha,
                    name,
                    idioma);
        }
    }
}

