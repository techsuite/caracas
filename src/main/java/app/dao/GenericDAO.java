package app.dao;


import app.dao.connector.Connector;
import app.model.response.Metadata;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Repository
public class GenericDAO {

    @Autowired
    protected Connector connector;

    @Autowired
    protected QueryRunner queryRunner;

    protected String paginationQuery(Metadata metadata){
        return String.format("LIMIT %d OFFSET %d ",
                metadata.getPageSize(),
                metadata.getPageSize()*(metadata.getPageNumber()-1));
    }

    protected String idiomaQuery(String idioma){
        return String.format("WHERE idioma = '%s' ",
                idioma
                );
    }

    protected String nombreQuery(String idioma){
        return String.format("and nombre = '%s' ",
                idioma
        );
    }

    public <T> List<T> getPagesFromQuery(Metadata metadata, String query, Class<T> type) throws Exception{
        List<T> result;
        String finalQuery = String.format("%s %s",
                query,
                paginationQuery(metadata)
                );
        try(Connection conn = connector.getConnection()){
            result = queryRunner.query(conn,finalQuery, new BeanListHandler<>(type));

        }
        return result;
    }


    public <T> List<T> getPagesWithIdioma(Metadata metadata,String idioma, String table, Class<T> type) throws Exception{
        String query = String.format("SELECT * FROM %s %s",table , idiomaQuery(idioma));

        return getPagesFromQuery(metadata, query, type);

    }

    public Integer numOfRows(String table, String idioma) throws Exception {
        Long result= (long) 0;
        String query = String.format("SELECT COUNT(*) FROM %s %s", table, idiomaQuery(idioma));
        try(Connection conn = connector.getConnection()){
            result = queryRunner.query(conn, query, new ScalarHandler<Long>());
        }

        return result.intValue();


    }

    public void deleteFromQuery(String Table, String nombre, String idioma) throws SQLException {
        String finalQuery = String.format("DELETE FROM %s %s %s", Table, idiomaQuery(idioma), nombreQuery(nombre));
        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn, finalQuery);

        }
    }

}
