package app.factory;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.util.Scanner;

@Component
public class WebDriverFactory {
  static {
    WebDriverManager.chromedriver().setup();
  }

  public WebDriver newChromeDriver() {
    return new ChromeDriver(new ChromeOptions().setHeadless(true));
  }

  public Scanner newScanner(File file) throws FileNotFoundException {
    return new Scanner(file);
  }

  public File newFile(String filename) throws FileNotFoundException {
    return new File(filename);
  }

  public WebDriverWait newWebDriverWait(WebDriver driver, Duration duration){
    return new WebDriverWait(driver, duration);
    
  }

}
