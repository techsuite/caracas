package app.factory;

import app.model.response.ListProdNatsResponse;
import app.model.response.Metadata;
import app.model.response.ProdNatResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProdNatResponseFactory {

    public ListProdNatsResponse newProdNatResponse(List<ProdNatResponse> productos_naturales, Metadata metadata){
        return new ListProdNatsResponse(productos_naturales, metadata);
    }

}