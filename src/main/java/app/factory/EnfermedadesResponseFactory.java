package app.factory;

import app.model.response.EnfermedadResponse;
import app.model.response.ListEnfermedadesResponse;
import app.model.response.Metadata;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EnfermedadesResponseFactory {

    public ListEnfermedadesResponse newEnfermedadesResponse(List<EnfermedadResponse> enfermedades, Metadata metadata){
        return new ListEnfermedadesResponse(enfermedades, metadata);
    }

}