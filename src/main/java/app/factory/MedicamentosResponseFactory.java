package app.factory;

import app.model.response.ListMedicamentosResponse;
import app.model.response.MedicamentoResponse;
import app.model.response.Metadata;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MedicamentosResponseFactory {

    public ListMedicamentosResponse newMedicamentosResponse(List<MedicamentoResponse> medicamentos, Metadata metadata){
        return new ListMedicamentosResponse(medicamentos, metadata);
    }

}
