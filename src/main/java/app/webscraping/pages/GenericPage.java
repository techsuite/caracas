package app.webscraping.pages;

import app.factory.WebDriverFactory;
import app.model.EnfermedadesModel;
import app.model.MedicamentosModel;
import app.model.ProdNatModel;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.util.List;


@Component
public class GenericPage {

  @Autowired
  private WebScraping webScraper;

  @Autowired
  private WebDriverFactory driverFactory;

  public List<MedicamentosModel> getDrugsList(String language, List<MedicamentosModel> medicamentos) throws FileNotFoundException {
    WebDriver driver = driverFactory.newChromeDriver();
    List<MedicamentosModel> drugsList = webScraper.getDrugs(driver, language, medicamentos);

    driver.close();

    return drugsList;
  }

  public List<ProdNatModel> getHerbsList(String language, List<ProdNatModel> productos) throws FileNotFoundException {
    WebDriver driver = driverFactory.newChromeDriver();
    List<ProdNatModel> herbsList = webScraper.getHerbs(driver, language, productos);

    driver.close();

    return herbsList;
  }

  public List<EnfermedadesModel> getIllnessesList(String language, List<EnfermedadesModel> enfermedades) throws FileNotFoundException {
    WebDriver driver = driverFactory.newChromeDriver();
    List<EnfermedadesModel> illnessesList = webScraper.getIllnesses(driver, language, enfermedades);

    driver.close();

    return illnessesList;
  }

}