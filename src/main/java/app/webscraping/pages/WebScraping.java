package app.webscraping.pages;

import app.factory.WebDriverFactory;
import app.model.EnfermedadesModel;
import app.model.MedicamentosModel;
import app.model.ProdNatModel;
import app.service.IOService;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;



@Component
public class WebScraping {

  //private WebDriver driver;
  @Autowired
  private WebDriverFactory webDriverFactory;


  public LinkedList<MedicamentosModel> getDrugs(WebDriver driver, String language, List<MedicamentosModel> medicamentos) throws FileNotFoundException {

    HashMap<String, String> sites = new HashMap<>();

    sites.put("spanish", "https://cima.aemps.es/cima/publico/home.html");
    sites.put("english", "https://medlineplus.gov/");

    LinkedList<MedicamentosModel> drugs = new LinkedList<>();

    for (final MedicamentosModel med : medicamentos) {
      try {
        drugs.addLast(getDrug(driver, sites.get(language), med));
      } catch (Exception e) {
        System.out.println(med.getNombre() + " not found\n");
      }
    }

    return drugs;
  }

  public LinkedList<ProdNatModel> getHerbs(WebDriver driver, String language, List<ProdNatModel> productos) throws FileNotFoundException {

    HashMap<String, String> sites = new HashMap<>();

    sites.put("spanish", "https://medlineplus.gov/spanish/");
    sites.put("english", "https://medlineplus.gov/");

    LinkedList<ProdNatModel> herbs = new LinkedList<>();

    for (final ProdNatModel prod : productos) {
      try {
        herbs.addLast(getHerb(driver, sites.get(language), prod));
      } catch (Exception e) {
        System.out.println(prod.getNombre() + " not found\n");
      }
    }

    return herbs;
  }

  public LinkedList<EnfermedadesModel> getIllnesses(WebDriver driver, String language, List<EnfermedadesModel> enfermedades) throws FileNotFoundException {

    HashMap<String, String> sites = new HashMap<>();

    sites.put("spanish", "https://www.merckmanuals.com/es-us/hogar");
    sites.put("english", "https://www.merckmanuals.com/home");

    LinkedList<EnfermedadesModel> illnesses = new LinkedList<>();

    for (final EnfermedadesModel enf : enfermedades) {
      try {
        illnesses.addLast(getIllness(driver, sites.get(language), enf));
      } catch (Exception e) {
        System.out.println(enf.getNombre() + " not found\n");
      }
    }

    return illnesses;
  }


  public MedicamentosModel getDrug(WebDriver driver, String url, MedicamentosModel drug) {
    String language = drug.getIdioma();
    String query = drug.getNombre();

    lookupSite(driver, url);


    if (language.equals("spanish")) {

      searchInput(driver, "//*[@id=\"inputbuscadorsimple\"]", query);
      clickButton(driver, "//*[@id=\"btnBuscarSimple\"]");

      waitAndClick(driver, "//*[@id=\"buttonp3\"]");

      switchToNewlyOpenedTab(driver);

      drug.setPosologia(getText(driver, "//*[@id=\"content\"]/div/div[2]/section[4]/div"));
      drug.setContraind(getText(driver, "//*[@id=\"content\"]/div/div[2]/section[5]/div"));
      drug.setComposicion(getText(driver, "//*[@id=\"content\"]/div/div[2]/section[7]/div"));
      drug.setUrl(driver.getCurrentUrl());
      drug.setProspecto(driver.getCurrentUrl());

    } else {



      lookupDrug(driver, url, query);


      drug.setUrl(driver.getCurrentUrl());
      drug.setProspecto(driver.getCurrentUrl());
      drug.setComposicion(query+ " composition:");
      drug.setContraind(getText(driver, "//*[@id=\"side-effects\"]"));
      drug.setPosologia(getText(driver, "//*[@id=\"how\"]"));

    }

    return drug;
  }

  public ProdNatModel getHerb(WebDriver driver, String url, ProdNatModel herb) {
    String query = herb.getNombre();
    String language = herb.getIdioma();

    lookupHerb(driver, url, query);

    herb.setPosologia(getText(driver, "//*[@id=\"section-8\"]"));
    herb.setContraind(getText(driver, "//*[@id=\"section-4\"]"));
    herb.setComposicion(getText(driver, "//*[@id=\"section-1\"]"));
    herb.setUrl(driver.getCurrentUrl());
    herb.setProspecto(driver.getCurrentUrl());

    return herb;
  }

  public EnfermedadesModel getIllness(WebDriver driver, String url,EnfermedadesModel illness) {
    String query = illness.getNombre();
    String language = illness.getIdioma();

    lookupIllness(driver, url, query);


    illness.setSintomas(getText(driver, "//*[@id='Symptoms']/../div"));
    illness.setRecomendaciones(getText(driver, "//*[@id='Treatment']/../div"));
    illness.setUrl(driver.getCurrentUrl());

    return illness;
  }

  private void acceptCookiesIfPossible(WebDriver driver) {
    if(driver.findElements(By.id("onetrust-banner-sdk")).size() > 0) {
      driver.findElement(By.id("onetrust-accept-btn-handler")).click();
    }
  }

  private void lookupDrug(WebDriver driver, String url, String query) {
    lookupSite(driver, url);

    searchInput(driver, "//*[@id=\"searchtext_primary\"]", query);
    clickButton(driver, "/html/body/div[1]/header/div/div[3]/form/div/div[2]/button");

    waitAndClick(driver, "//*[@class=\"document source-drugs\"]/div/a");
  }

  private void lookupHerb(WebDriver driver, String url, String query) {
    lookupSite(driver, url);

    searchInput(driver, "//*[@id=\"searchtext_primary\"]", query);
    clickButton(driver, "/html/body/div[1]/header/div/div[3]/form/div/div[2]/button");

    waitAndClick(driver, "/html/body/div[1]/div/div[4]/div/div[3]/div/div/div/ol/li[1]/div[1]/a");
  }

  private void lookupIllness(WebDriver driver, String url, String query) {
    lookupSite(driver, url);

    acceptCookiesIfPossible(driver);
    searchInput(driver, "/html/body/div[4]/div[1]/div[1]/div[1]/header/nav/div[2]/div[1]/form/div/div/input", query);

    clickButton(driver, "/html/body/div[4]/div[1]/div[1]/div[1]/header/nav/div[2]/div[1]/form/div/button");

    waitAndClick(driver, "//*[@class='search__result--main']/a");
  }

  private void lookupSite(WebDriver driver, String url) {
    driver.get(url);
  }

  private void searchInput(WebDriver driver, String xpath, String search) {
    driver.findElement(By.xpath(xpath)).sendKeys(search);
  }

  private void clickButton(WebDriver driver, String xpath) {
    driver.findElement(By.xpath(xpath)).click();
  }

  public String getText(WebDriver driver, String xpath) {
    return wait(driver, xpath).getText();
  }

  private void waitAndClick(WebDriver driver, String xpath) {
    wait(driver, xpath).click();
  }

  private WebElement wait(WebDriver driver, String xpath) {
    return webDriverFactory.newWebDriverWait(driver, Duration.ofSeconds(15)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
  }

  private void switchToNewlyOpenedTab(WebDriver driver) {
    ArrayList<String> tabs_windows = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(tabs_windows.get(tabs_windows.size() - 1));
  }
}