package app.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Solicitud {
    private String nombre;
    private String tipo;
    private int frecActu;
    private String idioma;
}
