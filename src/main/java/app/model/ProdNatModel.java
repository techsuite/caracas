package app.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class ProdNatModel {
    private String nombre = "";
    private String contraind, posologia, prospecto, composicion, url;
    private String idioma = "";
    private int  frecUpdateHours=2;//en dias en db
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)//para formato epoch
    private Timestamp dateNextUpdate;

    public ProdNatModel(String nombre,String idioma){
        this.nombre=nombre;
        this.idioma = idioma;
    }


}
