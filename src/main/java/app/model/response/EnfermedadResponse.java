package app.model.response;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class EnfermedadResponse {
    private String nombre;
    private String sintomas, recomendaciones, url, idioma;
}
