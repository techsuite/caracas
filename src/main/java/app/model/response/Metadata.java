package app.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Metadata {

  @NotNull
  private Integer pageNumber;

  @NotNull
  private Integer pageSize;

  private Integer totalPages;
}
