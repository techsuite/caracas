package app.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListMedicamentosResponse {

    private List<MedicamentoResponse> medicamentos;
    private Metadata metadata;

}
