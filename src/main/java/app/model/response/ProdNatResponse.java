package app.model.response;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class ProdNatResponse {
    private String nombre;
    private String contraind, posologia, prospecto, composicion, url;
    private String idioma;
}
