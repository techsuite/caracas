package app.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class EnfermedadesModel {
    private String nombre = "";
    private String sintomas, recomendaciones, url, idioma;
    private int  frecUpdateHours=2;//en dias en db
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)//para formato epoch
    private Timestamp dateNextUpdate;
    /**
     * Contructor
     * @param nombre
     * @param idioma
     */
    public EnfermedadesModel(String nombre, String idioma){
        this.nombre=nombre;
        this.idioma=idioma;
    }

}
