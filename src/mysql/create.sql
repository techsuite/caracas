#creamos la tabla medicamentos
CREATE TABLE `caracas`.`medicamentos` (
  `nombre` VARCHAR(45) NOT NULL,
  `contraind` LONGTEXT NULL DEFAULT NULL,
  `posologia` MEDIUMTEXT NULL DEFAULT NULL,
  `prospecto` MEDIUMTEXT NULL DEFAULT NULL,
  `composicion` TEXT NULL,
  `url` TEXT NULL DEFAULT NULL,
  `idioma` ENUM('spanish', 'english'),
  `frecUpdateHours` INT NULL DEFAULT 24,
  `dateNextUpdate` DATETIME(0) DEFAULT NULL,
  PRIMARY KEY (`nombre`, `idioma`));

#creamos la tabla productos naturales
CREATE TABLE `caracas`.`productos_naturales`(
  `nombre` VARCHAR(45) NOT NULL,
  `contraind` LONGTEXT NULL DEFAULT NULL,
  `posologia` MEDIUMTEXT NULL DEFAULT NULL,
  `prospecto` MEDIUMTEXT NULL DEFAULT NULL,
  `composicion` TEXT NULL,
  `url` TEXT NULL DEFAULT NULL,
  `idioma` ENUM('spanish', 'english'),
  `frecUpdateHours` INT NULL DEFAULT 24,
  `dateNextUpdate` DATETIME(0) DEFAULT NULL,
  PRIMARY KEY (`nombre`, `idioma`));

  #creamos la tabla enfermedades
  CREATE TABLE `caracas`.`enfermedades`(
    `nombre` VARCHAR(45) NOT NULL,
    `sintomas` MEDIUMTEXT NULL DEFAULT NULL,
    `recomendaciones` MEDIUMTEXT NULL DEFAULT NULL,
    `url` TEXT NULL DEFAULT NULL,
    `idioma` ENUM('spanish', 'english'),
    `frecUpdateHours` INT NULL DEFAULT 24,
    `dateNextUpdate` DATETIME(0) DEFAULT NULL,
    PRIMARY KEY (`nombre`, `idioma`));

