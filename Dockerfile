FROM techsuite/web-scraper

EXPOSE 8080

COPY . /usr/src/mymaven
WORKDIR /usr/src/mymaven
CMD ["sudo","mvn","spring-boot:run"]

